/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

/**
 *
 * @author Belguith
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import org.apache.commons.io.FileUtils;

public class UploadScreenController {

    @FXML
    TextField path;
    protected Window primaryStage;

    File file;

    @FXML
    protected void processBrowse(ActionEvent event) {

        FileChooser chooser = new FileChooser();
        file = chooser.showOpenDialog(primaryStage);

        if (file != null) {
            try {
                String str = file.getCanonicalPath();
                str = str.replace('\\', '/');
                path.setText(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(file.getName());

        } else {
            path.setText("Cancelled");
        }
    }

    @FXML
    protected void processUpload(ActionEvent event) throws IOException {

        //String target="C:\\ftpFiles\\";
        String fileName = file.getName();

        if (file != null) {

            URL ur = new URL("file://C:/wamp/www/FTP/images/");

            String hos = ur.getPath();

            System.out.println("home dir path is" + hos);

            String windPath = hos.replaceAll("\\\\", "/");

            System.out.println("windows path for copy is" + windPath);
            File targetFile = new File(hos + fileName);
            System.out.println("target ===> " + targetFile.getPath());
            FileUtils.copyFile(file, targetFile);
        }

    }

}
