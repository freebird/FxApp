/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Chapter;
import Entity.Course;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class CourseDetailsController implements Initializable {

    @FXML
    private ScrollPane courseDetailsScrollPane;
    @FXML
    private GridPane courseDetailsGridPane;
    @FXML
    private HBox topHbox;
    @FXML
    private Label titleCourseLabel;
    @FXML
    private Label publishedCourseLabel;
    @FXML
    private JFXButton coursebyButton;
    @FXML
    private ImageView courseInstructorAvatar;
    @FXML
    private TitledPane chaptersTitlePane;
    @FXML
    private ImageView chaptersIconImageView;
    @FXML
    private ScrollPane chapterScrollPane;
    @FXML
    private Label descriptionCourseLabel;
    @FXML
    private JFXTextArea commentCourseTextArea;
    @FXML
    private JFXButton submitCommentButton;
    @FXML
    private HBox awardsListHbox;
    @FXML
    private JFXButton subscribeToCourseButton;
    @FXML
    private WebView videoWebView;

    private HomeController parent;
    private Course cours;

    private ObservableList<Chapter> chapterList;

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public Course getCours() {
        return cours;
    }

    public void setCours(Course cours) {
        this.cours = cours;
    }

    private ObservableList<Chapter> getChapterList() {

        return App.factory.getChapiter().findBy("COURSE", cours.getId());
    }

    private void loadChapterDetails(Chapter chap) {
        ChapterDetailsController cdc = new ChapterDetailsController();
        URL location = ChapterDetailsController.class.getResource("/GUI/chapterDetails.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        cdc.setParent(this.getParent());
        cdc.setCourse(this.getCours());

        cdc.setChapter(App.factory.getChapiter().findById(chap.getId()));
        fxmlLoader.setController(cdc);
        Parent root;
        try {
            System.out.println("loading profile.fxml");
            root = fxmlLoader.load();
            parent.getCentralScrollPane().setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(ChapterDetailsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private VBox createChaptersVBOX() {
        chapterList = getChapterList();
        VBox chaptersVBox = new VBox();

        chapterList.stream().forEach((Chapter c) -> {
            HBox courseHBox = new HBox(10);
            courseHBox.getStyleClass().add("coursehbox");
            courseHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

            JFXButton showChapterButton = new JFXButton("Show");
            showChapterButton.getStyleClass().add("hboxbutton");
            showChapterButton.setAlignment(Pos.CENTER);
            showChapterButton.setOnAction((ActionEvent e) -> {
                System.out.println("show me this chapter \n" + c.toString());
                loadChapterDetails(c);

            });

            ImageView icon = new ImageView(cours.getIcon().getPath());
            icon.setFitHeight(50);
            icon.setPreserveRatio(true);

            Label title = new Label(c.getTitle().toUpperCase());

            //Label rate = new Label("" + c.getRate());
            title.getStyleClass().add("coursetitle");
            // rate.getStyleClass().add("coursetitle");

            Region r = new Region();
            r.setPrefWidth(30);
            r.setPrefHeight(10);
            r.setMinWidth(10);

            courseHBox.setHgrow(r, Priority.SOMETIMES);

            courseHBox.getChildren().addAll(icon, title, /*rate,*/ r, showChapterButton);

            chaptersVBox.getChildren().add(courseHBox);
        });

        return chaptersVBox;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        chapterScrollPane.setContent(createChaptersVBOX());
        titleCourseLabel.setText(cours.getTitle());
        //publishedCourseLabel.setText(cours.getPublished().toString());
        coursebyButton.setText("By " + cours.getInstructor().getFirstName());
        courseInstructorAvatar.setImage(new Image(cours.getInstructor().getAvatar().getPath()));
        videoWebView.getEngine().setJavaScriptEnabled(true);
        videoWebView.getEngine().load(cours.getVideo().getUrl());
        descriptionCourseLabel.setText(cours.getDescription());
    }

}
