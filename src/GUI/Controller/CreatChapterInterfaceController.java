/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Chapter;
import Entity.Course;
import Entity.Media;
import Entity.Video;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.media.MediaView;
import javafx.scene.shape.Circle;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class CreatChapterInterfaceController implements Initializable {

    @FXML
    private ScrollPane createChapiterScrollPane;
    @FXML
    private GridPane organismDetailsGridPane;
    @FXML
    private JFXButton editChapiterButton;
    @FXML
    private JFXTextField chapiterTitleTextField;
    @FXML
    private JFXTextField videoTextField;
    @FXML
    private JFXTextArea chapiterDescriptionTextArea;
    @FXML
    private JFXTextArea chapiterContentTextArea;
    @FXML
    private JFXButton chapiterPresentationButton;
    @FXML
    private Circle chapiterPresentationCircle;
    @FXML
    private JFXButton chapiterVideoButton;
    @FXML
    private MediaView chapiterVideoMediaView;
    @FXML
    private JFXSlider levelChapterSlider;
    @FXML
    private Label levelChapterLabel;

    @FXML
    private JFXButton chapiterBookButton;
    @FXML
    private JFXButton confirmChapiterEditButton;
    @FXML
    private TitledPane quizzTitlePane;
    @FXML
    private ImageView chaptersIconImageView;
    @FXML
    private ScrollPane chaptersScrollPane;
    @FXML
    private TableView<?> quizzTableView;
    @FXML
    private TableColumn<?, ?> quizzIdColumn;
    @FXML
    private TableColumn<?, ?> quizzLevelColumn;
    @FXML
    private TableColumn<?, ?> quizzTypeColumn;
    @FXML
    private TableColumn<?, ?> quizzAwardColumn;
    @FXML
    private JFXButton newQuizzButton;
    @FXML
    private JFXTextArea chapiterPresentationTextArea;
    @FXML
    private JFXCheckBox availableChapterCheckBox;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Chapter chapter = new Chapter();

        confirmChapiterEditButton.setOnAction((ActionEvent e) -> {
            chapter.setTitle(chapiterTitleTextField.getText());
            chapter.setDescription(chapiterDescriptionTextArea.getText());
            chapter.setVideo(videoTextField.getText());
            chapter.setPresentation(chapiterPresentationTextArea.getText());
            chapter.setContent(chapiterContentTextArea.getText());
            if (availableChapterCheckBox.isSelected() == true) {
                chapter.setAvailable(1);
            } else if (availableChapterCheckBox.isSelected() == false) {
                chapter.setAvailable(0);
            }

            App.factory.getChapiter().insert(chapter);
        });

    }

}
