/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.User;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class LoginInterfaceController implements Initializable {

    @FXML
    private VBox loginVbox;
    @FXML
    private JFXTextField emailTextField;
    @FXML
    private JFXPasswordField passwordField;
    @FXML
    private Hyperlink forgotPasswordLink;
    @FXML
    private HBox connectHBox;
    @FXML
    private JFXButton loginButton;

    @FXML
    private ImageView logoImageView;

    public static boolean TryConnection(String username, String password) {
        String result = "";
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost("http://localhost/WebApp/web/app_dev.php/checkpassword");

        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
        params.add(new BasicNameValuePair("username", username));
        params.add(new BasicNameValuePair("password", password));
        try {
            httppost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
                    StringBuilder out = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append(line);
                    }
                    result = out.toString();

                    System.out.println(result);

                    reader.close();
                } finally {
                    instream.close();
                }
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (result.equals("OK")) {
            return true;
        } else {
            return false;
        }
    }

    private User loadLogginUser() {
        User u;

        String emailInput = emailTextField.getText();
        String passwordInput = passwordField.getText();

        if (TryConnection(emailInput, passwordInput) == true) {
            System.out.println("user OK");
            u = App.factory.getUser().findOneBy("email", emailInput);

            return u;
        }
        return null;

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("initialize longin interface");

        loginButton.setContentDisplay(ContentDisplay.RIGHT);
    }

    /// Load the Home Window empty ...
    // Home Contend depends on  logged User Role //
    private void loadHome() {
        URL location = HomeController.class.getResource("/GUI/home.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root;
        try {
            System.out.println("Loadgin Home==>" + App.user.getUsername());
            root = fxmlLoader.load();

            Scene scene = new Scene(root);

            App.window.setScene(scene);

            App.window.centerOnScreen();
            App.window.show();

        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void loginAction(ActionEvent event) {
        App.user = loadLogginUser();
        if (App.user.isEnabled() == true) {
            System.out.println("Enabled ");
            loadHome();

        }
    }

}
