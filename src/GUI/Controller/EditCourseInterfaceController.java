/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Chapter;
import Entity.Course;
import Entity.Media;
import Entity.Video;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class EditCourseInterfaceController implements Initializable {

    @FXML
    private ScrollPane courseDetailsScrollPane;
    @FXML
    private GridPane courseDetailsGridPane;
    @FXML
    private JFXTextField titleCourseTextField;
    @FXML
    private JFXSlider levelCourseSlider;
    @FXML
    private Label levelCourseLabel;
    @FXML
    private JFXButton confirmCourseEditButton;
    @FXML
    private JFXButton addChapterEditButton;
    @FXML
    private ScrollPane chaptersScrollPane;

    @FXML
    private JFXButton cancelButton;

    private BrowseCoursesController parent;

    public BrowseCoursesController getParent() {
        return parent;
    }

    public void setParent(BrowseCoursesController parent) {
        this.parent = parent;
    }

    private Course cours;

    public Course getCours() {
        return cours;
    }

    public void setCours(Course cours) {
        this.cours = cours;
    }

    private ObservableList<Chapter> getChapterList() {

        return App.factory.getChapiter().findBy("COURSE", cours.getId());
    }

    private VBox createChaptersVBOX() {

        VBox chaptersVBox = new VBox();

        getChapterList().stream().forEach((Chapter c) -> {
            HBox courseHBox = new HBox(10);
            courseHBox.getStyleClass().add("coursehbox");
            courseHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

            JFXButton showChapterButton = new JFXButton("Edit");
            showChapterButton.getStyleClass().add("hboxbutton");
            showChapterButton.setAlignment(Pos.CENTER);
            showChapterButton.setOnAction((ActionEvent e) -> {
                System.out.println("show me this chapter \n" + c.toString());
                // loadChapterDetails(c);

            });

            ImageView icon = new ImageView(cours.getIcon().getPath());
            icon.setFitHeight(50);
            icon.setPreserveRatio(true);

            Label title = new Label(c.getTitle().toUpperCase());

            //Label rate = new Label("" + c.getRate());
            title.getStyleClass().add("coursetitle");
            // rate.getStyleClass().add("coursetitle");

            Region r = new Region();
            r.setPrefWidth(30);
            r.setPrefHeight(10);
            r.setMinWidth(10);

            courseHBox.setHgrow(r, Priority.SOMETIMES);

            courseHBox.getChildren().addAll(icon, title, /*rate,*/ r, showChapterButton);

            chaptersVBox.getChildren().add(courseHBox);
        });

        return chaptersVBox;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chaptersScrollPane.setContent(createChaptersVBOX());

        titleCourseTextField.setText(cours.getTitle());
        levelCourseSlider.setValue((double) cours.getLevel());
        levelCourseLabel.setText("" + cours.getLevel());
        confirmCourseEditButton.setOnAction((ActionEvent e) -> {
            cours.setTitle(titleCourseTextField.getText());
            cours.setLevel((int) levelCourseSlider.getValue());

            App.factory.getCourse().update(cours);
        });
        levelCourseSlider.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                levelCourseLabel.setText("" + levelCourseSlider.getValue()); //To change body of generated methods, choose Tools | Templates.
            }
        });
        addChapterEditButton.setOnAction((ActionEvent e) -> {

            System.out.println("hello");
        });
        cancelButton.setOnAction((ActionEvent e) -> {
            this.parent.initInterface();
        });
    }

}
