/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Course;
import Entity.Media;
import Entity.Video;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.File;
import java.io.IOException;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class CreateCourseInterfaceController implements Initializable {
    
    @FXML
    private ScrollPane courseDetailsScrollPane;
    @FXML
    private GridPane courseDetailsGridPane;
    @FXML
    private JFXTextField titleCourseTextField;
    @FXML
    private JFXTextField videoTextField;
    @FXML
    private JFXTextArea descriptionCourseTextArea;
    @FXML
    private JFXSlider levelCourseSlider;
    @FXML
    private Label levelCourseLabel;
    @FXML
    private JFXButton videoCourseButton;
    @FXML
    private JFXSlider durationCourseSlider;
    @FXML
    private Label dirationCourseLabel;
    @FXML
    private JFXButton confirmCourseEditButton;
    @FXML
    private JFXButton bookCourseButton;
    @FXML
    private JFXButton cancelBtn;
    @FXML
    private JFXButton iconBtn;
    
    private Course course;
    private Media m;
    private Video v;
    private File file;
    
    @FXML
    private Circle circleImg;
    
    public Course getCourse() {
        return course;
    }
    
    public void setCourse(Course course) {
        this.course = course;
    }
    
    private BrowseCoursesController parent;
    
    public BrowseCoursesController getParent() {
        return parent;
    }
    
    public void setParent(BrowseCoursesController parent) {
        this.parent = parent;
    }
    
    public void initInterface() {
        
        durationCourseSlider.setOnMouseDragReleased((MouseEvent e) -> {
            dirationCourseLabel.setText("" + durationCourseSlider.getValue());
            
        });
        
        levelCourseSlider.setOnMouseDragReleased((MouseEvent event) -> {
            levelCourseLabel.setText("" + levelCourseSlider.getValue()); //To change body of generated methods, choose Tools | Templates.
        });
        
        cancelBtn.setOnAction((ActionEvent e) -> {
            this.parent.initInterface();
        });
        
        confirmCourseEditButton.setOnAction((ActionEvent e) -> {
            File source = new File("http://localhost/FTP/images/book.png");
            File dest = new File("http://localhost/FTP/images/" + titleCourseTextField.getText());
            
            try {
                FileUtils.copyDirectory(source, dest);
            } catch (IOException ex) {
                Logger.getLogger(CreateCourseInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        
        iconBtn.setOnAction((ActionEvent e) -> {
            Image img = new Image("http://localhost/FTP/images/book.png");
            circleImg.setFill(new ImagePattern(img));
            m = new Media(0, titleCourseTextField.getText(), "http://localhost/FTP/images/book.png");
            
        });
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initInterface();
        this.course = new Course();
        
        confirmCourseEditButton.setOnAction((ActionEvent e) -> {
//            course.setTitle(titleCourseTextField.getText());
//            course.setDescription(descriptionCourseTextArea.getText());
//            course.setLevel((int) levelCourseSlider.getValue());
//            //App.factory.getMedia().insert();
//            App.factory.getVideo().insert(course.getVideo());
//            course.setIcon(new Media(0, course.getTitle(), null));
//            course.setVideo(new Video(0, course.getTitle(), videoTextField.getText()));
//            course.setBook("http://www.fsr.ac.ma/cours/informatique/rziza/cours/Introduction_Reseau.pdf");
//            course.setDuration(date);
//            App.factory.getCourse().insert(course);
            this.course = new Course();
            this.course.setTitle(titleCourseTextField.getText());
            this.course.setLevel(toIntExact(round(levelCourseSlider.getValue())));
            
            Calendar cal = Calendar.getInstance();
            java.util.Date date;
            LocalDateTime ldt = LocalDateTime.now();
            int x = (int) (ldt.getHour() + durationCourseSlider.getValue());
            cal.set(ldt.getYear(), ldt.getMonthValue(), ldt.getDayOfMonth(), x, ldt.getMinute());
            date = cal.getTime();
            this.course.setDuration(date);
            
            this.course.setVideo(new Video(0, titleCourseTextField.getText(), videoTextField.getText()));
            this.course.setDescription(descriptionCourseTextArea.getText());
            this.course.setAvailable(0);
            this.course.setIcon(m);
            course.setVideo(new Video(0, course.getTitle(), "https://www.youtube.com/embed/lisiwUZJXqQ"));
            
            App.factory.getVideo().insert(course.getVideo());
            course.getVideo().setId(App.factory.getVideo().findOneBy("URL", "https://www.youtube.com/embed/lisiwUZJXqQ").getId());
            App.factory.getMedia().insert(m);
            course.getIcon().setId(App.factory.getMedia().findOneBy("path", "http://localhost/FTP/images/book.png").getId());
            course.getInstructor().setId(App.user.getId());
            App.factory.getCourse().insert(course);
        });
        
    }
    
}
