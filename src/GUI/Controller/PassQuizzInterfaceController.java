/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Question;
import Entity.Mark;
import Entity.Quizz;
import com.jfoenix.controls.JFXButton;
import fxapp.App;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Rim
 */
public class PassQuizzInterfaceController implements Initializable {

    private static final Integer STARTTIME = 15;
    private Timeline timeline;
    @FXML
    private Label timerLabel = new Label();
    @FXML
    private JFXButton submitAnswer;
    @FXML
    private VBox quizzVBox;

    private IntegerProperty timeSeconds
            = new SimpleIntegerProperty(STARTTIME);

    @FXML
    Label statement_label;
    @FXML
    CheckBox rep1_cb;
    @FXML
    CheckBox rep2_cb;
    @FXML
    CheckBox rep3_cb;
    @FXML
    CheckBox rep4_cb;
    Integer IdQuestion = 0;
    Integer Note = 0;
    List<CheckBox> CL = new ArrayList();
    ObservableList<Question> Questions = App.factory.getQuestion().findBy("quizz_id", 22);
    private HomeController parent;
    private Quizz quiz;

    public void setQuiz(Quizz quiz) {
        this.quiz = quiz;
    }

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    /**
     * Initializes the controller class.
     */
    private void loadQuestion(int index) {

        statement_label.setText(Questions.get(index).getStatement());
        CL.add(rep1_cb);
        CL.add(rep2_cb);
        CL.add(rep3_cb);
        CL.add(rep4_cb);

////        
////        q.getResponse().stream().forEach((i) -> {
////            
////            rep1_cb.setText(i.getStatement());
////            rep2_cb.setText(i);
////            
////        });
        for (int i = 0; i < Questions.get(index).getResponse().size(); i++) {
            CL.get(i).setText(Questions.get(index).getResponse().get(i).getStatement());
        }

        timer();
        IdQuestion++;

    }

    public void SubmitAnswer() {
        Question q = App.factory.getQuestion().findById(IdQuestion - 1);

        List<String> SelectedAnswers;
        ObservableList<String> CorrectResponses = App.factory.getResponse().findValidResponse("question_id", IdQuestion - 1);
        SelectedAnswers = new ArrayList();

        for (int i = 0; i < 4; i++) {
            if (CL.get(i).isSelected()) {
                SelectedAnswers.add(CL.get(i).getText());
            }
        }
        System.out.println(SelectedAnswers.get(0));
        if (SelectedAnswers.equals(CorrectResponses)) {
            Note++;
            System.out.println(Note);
        }
        System.out.println(Note);
        if (IdQuestion < Questions.size()) {
            loadQuestion(IdQuestion);
        }
        if (IdQuestion == Questions.size()) {
            System.out.println("ok");
            Mark mark = new Mark();
            mark.setValue(Note);
            mark.setUser(App.user);
            quiz = App.factory.getQuizz().findById(1);
            mark.setQuizz(quiz);
            App.factory.getMarkDAO().insert(mark);
        }

    }

    public void timer() {
        timerLabel.textProperty().bind(timeSeconds.asString());
        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();

        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME + 1),
                        new KeyValue(timeSeconds, 0)));

        timeline.playFromStart();

        timeline.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                loadQuestion(IdQuestion);
            }
        });

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        loadQuestion(IdQuestion);
        submitAnswer.setOnAction((ActionEvent e) -> {
            SubmitAnswer();

        });

    }

}
