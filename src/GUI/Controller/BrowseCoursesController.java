/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Comment;
import Entity.Course;
import Entity.Instructor;
import Entity.Organism;
import Entity.Request;
import Entity.Student;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class BrowseCoursesController implements Initializable {

    @FXML
    private GridPane coursesCentralGridPane;
    @FXML
    private JFXComboBox<String> categoryComboBox;
    @FXML
    private JFXTextField searchTop;
    @FXML
    private JFXButton searchTopButton;
    @FXML
    private ImageView CoursesSearchImageView;
    @FXML
    private VBox centralVbox;
    @FXML
    private TitledPane topItemTitlePane;

    @FXML
    private ScrollPane topItemScrollPane;
    @FXML
    private VBox topInstructorVbox;
    @FXML
    private JFXButton cancelBtn;
    @FXML
    private ScrollPane topInstructorScrollPane;
    @FXML
    private HBox topHBOX;
    private TitledPane requestsTitlePane;
    private TitledPane commentsTitlePane;
    private VBox actionsVBox;

    private ObservableList<Course> topCoursesList;
    private ObservableList<Instructor> topInstructorsList;
    private HomeController parent;
    private Student student;
    private Instructor instructor;
    private Organism organism;
    private ObservableList<Request> userRequest;
    private ObservableList<Course> userCourses;
/// private ObsevableList<Comment> userComments;

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Organism getOrganism() {
        return organism;
    }

    public void setOrganism(Organism organism) {
        this.organism = organism;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public HomeController getParent() {
        return parent;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ObservableList<Course> getTopCourses() {

        return App.factory.getCourse().findTopCourses();

    }

    public ObservableList<Instructor> getTopInstructor(int nbr) {
        return App.factory.getInsctructor().findTopInstructor(nbr);
    }

    public HBox createInstructorHbox(Instructor i) {
        HBox h = new HBox(5);
        h.getStyleClass().add("topItemHbox");
        ImageView img;
        if (i.getAvatar().getPath() != null) {
            img = new ImageView(i.getAvatar().getPath());
        } else {
            img = new ImageView("http://localhost/FTP/images/book-bookmark-icon.png");
        }
        img.setPreserveRatio(true);
        img.setFitHeight(40);

        Label name = new Label(i.getUsername());
        name.getStyleClass().add("top-item-label");
        Label subs = new Label("" + i.getRate());
        ImageView like = new ImageView("http://localhost/FTP/images/like.png");
        like.setPreserveRatio(true);
        like.setFitHeight(24);
        subs.setGraphic(like);
        subs.setContentDisplay(ContentDisplay.LEFT);
        Region r = new Region();
        r.setPrefWidth(50);
        HBox.setHgrow(name, Priority.ALWAYS);
        h.getChildren().addAll(img, name, r, subs);

        h.setOnMouseClicked((MouseEvent e) -> {
            System.out.println("top instructor cliked" + i.getUsername());
        });
        return h;
    }

    public HBox createCourseHbox(Course c) {
        HBox h = new HBox(20);
        Label title = new Label(c.getTitle());
        Label subs = new Label(c.getSubscription().size() + "");
        h.getStyleClass().add("topItemHbox");
        ImageView icon = new ImageView("http://localhost/FTP/images/book-bookmark-icon.png"); //c.getIcon().getPath()
        title.getStyleClass().add("top-item-label");
        icon.setPreserveRatio(true);
        icon.setFitHeight(35);
        Region r = new Region();
        r.setPrefWidth(50);
        h.getChildren().addAll(icon, title, r, subs);
        HBox.setHgrow(r, Priority.ALWAYS);

        h.setOnMouseClicked((MouseEvent e) -> {
            System.out.println("top course cliked" + c.getTitle());
        });

        return h;
    }

    public VBox loadTopCourse() {
        VBox box = new VBox(1);
        topCoursesList = FXCollections.observableArrayList();
        topCoursesList = getTopCourses();
        topCoursesList.stream().forEach((c) -> {
            box.getChildren().add(createCourseHbox(c));

        });

        return box;
    }

    public VBox loadTopInstructor(int nbr) {
        VBox v = new VBox(1);
        topInstructorsList = FXCollections.observableArrayList();
        topInstructorsList = getTopInstructor(nbr);
        topInstructorsList.stream().forEach((i) -> {
            v.getChildren().add(createInstructorHbox(i));
        });
        v.setFillWidth(true);

        return v;
    }

    //// Requests
    public ObservableList<Request> getInstructorRequestList() {
        return App.factory.getRequest().findBy("INSTRUCTOR", App.user.getId());
    }

    public ObservableList<Request> getOrganismRequestList() {
        return App.factory.getRequest().findBy("ORGANISM", App.user.getId());
    }

    public HBox createRequestHbox(Request r) {
        HBox h = new HBox(5);
        h.getStyleClass().add("topItemHbox");
        ImageView img = new ImageView("http://localhost/FTP/images/user.png");
        JFXButton ok;
        JFXButton ko;
        Label name = new Label();

        if ((r.getType() == 1) && (r.getStatus() == 0)) {
            Instructor i = App.factory.getInsctructor().findById(r.getInstructor().getId());
            System.out.println("from org to inst");
            img = new ImageView(i.getAvatar().getPath());
            name = new Label(i.getUsername());

        } else if ((r.getType() == 0) && (r.getStatus() == 0)) {
            System.out.println("from inst to org");
            Organism o = App.factory.getOrganism().findById(r.getOrganism().getId());
            System.out.println("organism ==> " + o.getId());
            img = new ImageView(o.getAvatar().getPath());
            name = new Label(o.getUsername());

        }

        ok = new JFXButton("");
        ok.setOnAction((ActionEvent e) -> {
            System.out.println("request inst" + r.getInstructor().getId());
            //Set the request to status 1
        });
        ko = new JFXButton("");

        ko.setOnAction((ActionEvent e) -> {
            System.out.println("request inst" + r.getInstructor().getId());
            // set status to 2
        });

        img.setFitWidth(47);
        img.setPreserveRatio(true);
        img.getStyleClass().add("top-item-icon");
        name.setPrefWidth(Double.MAX_VALUE);
        name.getStyleClass().add("top-item-label");
        ok.getStyleClass().addAll("jfx-button", "ok-button");
        ko.getStyleClass().addAll("jfx-button", "ko-button");

        ok.setOnMouseClicked((MouseEvent e) -> {
            // TODO .. Set Status 1 Accepted
        });

        ko.setOnMouseClicked((MouseEvent e) -> {
            // TODO .. Set Status 2 Refused
        });

        h.setMinHeight(50);
        h.setMaxHeight(50);
        h.setMaxWidth(Double.MAX_VALUE);
        HBox.setHgrow(name, Priority.ALWAYS);
        h.getChildren().addAll(img, name, ok, ko);
        h.setOnMouseClicked((MouseEvent e) -> {
            System.out.println("i hover the Request between" + r.getInstructor().getId() + " and " + r.getOrganism().getId());
            // set status to 2
        });
        return h;
    }

    public VBox createRequestsVBOX(ObservableList<Request> list) {
        VBox v = new VBox(2);
        list.stream().forEach((r) -> {
            if ((r.getSeen() == 0) && (r.getStatus() == 0)) {
                System.out.println("Request ===>" + r.getContent());
                v.getChildren().add(createRequestHbox(r));
            }
        });

        return v;
    }

    public TitledPane createTitlePane(VBox vbox, String title) {
        ScrollPane sc = new ScrollPane();

        sc.setContent(vbox);
        sc.setFitToHeight(true);
        sc.setFitToWidth(true);
        sc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        sc.getStyleClass().add("myscrollpane");
        TitledPane t = new TitledPane();
        t.setContent(sc);
        t.setText(title);
        t.setAnimated(true);
        t.getStyleClass().addAll("title-pane", "top-title-pane");
        t.setPrefSize(180, 200);
        HBox.setHgrow(t, Priority.ALWAYS);
        return t;
    }

    ///Comments
    public ObservableList<Comment> getInstructorComments() {
        return App.factory.getComment().findByInstructorCourse(App.user.getId());
    }

    public HBox createCommentHBox(Comment c) {
        HBox h = new HBox(10);
        h.getStyleClass().add("topItemHbox");
        ImageView img = null;
        Label l;
        if (c.getPicture() == null) {
            img = new ImageView("../img/user.png");
        } else {
            img = new ImageView(c.getPicture());
        }

        img.setFitHeight(47);
        img.setPreserveRatio(true);
        l = new Label(c.getFullname() + " Commented on your course");

        img.setFitWidth(47);
        img.setPreserveRatio(true);
        img.getStyleClass().add("top-item-icon");
        l.setWrapText(true);
        l.setPrefWidth(Double.MAX_VALUE);
        l.getStyleClass().add("top-item-label");
        l.setMinWidth(130);
        h.getChildren().addAll(img, l);
        h.setOnMouseClicked((MouseEvent e) -> {
            System.out.println("i cliced the Comment " + c.getId() + " from course " + c.getCourse().getId());

            this.loadCourseDetails(App.factory.getCourse().findById(c.getCourse().getId()));
        });
        return h;
    }

    public VBox createCommentVBox(ObservableList<Comment> list) {
        VBox v = new VBox(2);
        list.stream().forEach((c) -> {
            if (c.getCreated() != c.getModified()) {
                System.out.println("Quelqun a commenté vote commentaire ou votre cours");
                v.getChildren().add(createCommentHBox(c));
            }
        });

        return v;

    }

    public void loadCourseDetails(Course course) {

        CourseDetailsController cdc = new CourseDetailsController();
        URL location = InstructorProfileController.class.getResource("/GUI/courseDetails.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        cdc.setParent(this.getParent());

        cdc.setCours(App.factory.getCourse().findById(course.getId()));
        fxmlLoader.setController(cdc);
        Parent root;
        try {
            root = fxmlLoader.load();
            // parent.getCentralScrollPane().getContent().
            parent.getCentralScrollPane().setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(CourseDetailsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public VBox createActionsVBox() {

        VBox v = new VBox(5);

        JFXComboBox<Course> combo = new JFXComboBox();
        JFXButton editBtn = new JFXButton("Edit");
        JFXButton addCourseBtn = new JFXButton("New Course");
        addCourseBtn.setOnAction((ActionEvent e) -> {
            System.out.println("Add new Course");
            //topHBOX.getChildren().get(1).setTranslateZ(0);
            ScaleTransition tt = new ScaleTransition(Duration.millis(1000), topHBOX.getChildren().get(0));
            tt.setFromX(1);
            tt.setFromY(1);
            tt.setToX(0);
            tt.setToY(0);
            tt.playFromStart();
            tt.setNode(topHBOX.getChildren().get(1));
            tt.playFromStart();
            topHBOX.getChildren().remove(requestsTitlePane);
            topHBOX.getChildren().remove(commentsTitlePane);
            createCreateCourseInterface();
            //todo load create course interface
        });

        editBtn.setOnAction((ActionEvent e) -> {
            System.out.println("Edit the course " + combo.getValue());

            ScaleTransition tt = new ScaleTransition(Duration.millis(1000), topHBOX.getChildren().get(0));
            tt.setFromX(1);
            tt.setFromY(1);
            tt.setToX(0);
            tt.setToY(0);
            tt.playFromStart();
            tt.setNode(topHBOX.getChildren().get(1));
            tt.playFromStart();
            topHBOX.getChildren().remove(requestsTitlePane);
            topHBOX.getChildren().remove(commentsTitlePane);
            createEditCours(combo.getValue());
        });

        combo.setPlaceholder(new Label("Choisir un Cours"));
        combo.setItems(userCourses);
        combo.setMinWidth(100);
        combo.setOnAction((ActionEvent e) -> {

            if (!v.getChildren().contains(editBtn)) {
                System.out.println("V dont contains editBtn");
                // Try to add Animation 
                v.getChildren().add(editBtn);
            }

        });

        v.getChildren().addAll(addCourseBtn, combo);

        return v;
    }

    public VBox createCreateCourseInterface() {
        VBox v = new VBox();

        CreateCourseInterfaceController spc = new CreateCourseInterfaceController();
        URL location = CreateCourseInterfaceController.class.getResource("/GUI/createCourseInterface.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        spc.setParent(this);

        fxmlLoader.setController(spc);
        Parent root;
        try {

            root = fxmlLoader.load();
            root.setTranslateZ(2);
            topHBOX.getChildren().add(root);
            topHBOX.getChildren().remove(0, 1);
            ScaleTransition ft = new ScaleTransition(Duration.millis(500), root);
            ft.setFromY(0);
            ft.setFromX(0);
            ft.setToY(1);
            ft.setToX(1);
            ft.setCycleCount(1);
            ft.setAutoReverse(true);
            ft.play();
        } catch (IOException ex) {
            Logger.getLogger(CreateCourseInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return v;
    }

    public VBox createEditCours(Course c) {
        VBox v = new VBox();

        EditCourseInterfaceController spc = new EditCourseInterfaceController();
        URL location = EditCourseInterfaceController.class.getResource("/GUI/editCourseInterface.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        spc.setCours(c);
        spc.setParent(this);

        fxmlLoader.setController(spc);
        Parent root;
        try {

            root = fxmlLoader.load();
            root.setTranslateZ(2);
            topHBOX.getChildren().add(root);
            topHBOX.getChildren().remove(0, 1);
            ScaleTransition ft = new ScaleTransition(Duration.millis(500), root);
            ft.setFromY(0);
            ft.setFromX(0);
            ft.setToY(1);
            ft.setToX(1);
            ft.setCycleCount(1);
            ft.setAutoReverse(true);
            ft.play();
        } catch (IOException ex) {
            Logger.getLogger(StudentProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return v;
    }

    public void initInterface() {

        topHBOX.getChildren().remove(0, topHBOX.getChildren().size());

        if (App.user.getRoles().contains("INSTRUCTOR")) {
            userRequest = getInstructorRequestList();
            userCourses = App.factory.getCourse().findBy("INSTRUCTOR", App.user.getId());
            requestsTitlePane = createTitlePane(createRequestsVBOX(userRequest), "Request");
            commentsTitlePane = createTitlePane(createCommentVBox(getInstructorComments()), "Comments");
            actionsVBox = createActionsVBox();
            topHBOX.getChildren().add(0, requestsTitlePane);
            topHBOX.getChildren().add(1, commentsTitlePane);
            topHBOX.getChildren().add(2, actionsVBox);

// this is a simple formatting standardised creation of a title pane like we want in our mooc TitlePane(Vbox(ObservableList Hbox(object)
        } else if (App.user.getRoles().contains("ORGANISM")) {
            userRequest = getOrganismRequestList();
            //Try to Add animations 
            requestsTitlePane = createTitlePane(createRequestsVBOX(userRequest), "Request");
            topHBOX.getChildren().add(0, requestsTitlePane);
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //initInterface();
        categoryComboBox.getItems().addAll("Course", "Instructor", "Organism");
        coursesCentralGridPane.getStylesheets().add("GUI/css/style.css");
        topInstructorScrollPane.getStylesheets().add("GUI/css/style.css");
        topItemScrollPane.getStylesheets().add("GUI/css/style.css");
        VBox topCoursesVBox = loadTopCourse();
        topItemScrollPane.setPrefHeight(150);
        topItemScrollPane.setContent(topCoursesVBox);

        VBox topIntructorVBox = loadTopInstructor(4);
        topInstructorScrollPane.setContent(topIntructorVBox);
        initInterface();
    }

}
