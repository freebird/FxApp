/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Chapter;
import Entity.Course;
import Entity.Quizz;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import fxapp.App;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class EditChapterInterfaceController implements Initializable {

    @FXML
    private ScrollPane createChapiterScrollPane;
    @FXML
    private JFXTextArea chapiterContentTextArea;
    @FXML
    private JFXCheckBox availableChapterCheckBox;
    @FXML
    private JFXButton confirmChapiterEditButton;
    @FXML
    private JFXButton addQuizzButton;
    @FXML
    private ScrollPane quizzScrollPane;
    @FXML
    private Label chapterNameLabel;

    private BrowseCoursesController parent;

    public BrowseCoursesController getParent() {
        return parent;
    }

    public void setParent(BrowseCoursesController parent) {
        this.parent = parent;
    }

    private Chapter chapter;
    private Course course;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    private ObservableList<Quizz> getQuizzList() {
        return App.factory.getQuizz().findBy("CHAPTER", chapter.getId());
    }

    private VBox createQuizzVBOX() {

        VBox quizzVBox = new VBox();

        getQuizzList().stream().forEach((Quizz c) -> {
            HBox quizzHBox = new HBox(10);
            quizzHBox.getStyleClass().add("quizzhbox");
            quizzHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

            JFXButton showChapterButton = new JFXButton("Edit");
            showChapterButton.getStyleClass().add("hboxbutton");
            showChapterButton.setAlignment(Pos.CENTER);
            showChapterButton.setOnAction((ActionEvent e) -> {
                System.out.println("show me this quizzz !! " + c.toString());
            });

            ImageView icon = new ImageView(c.getSkill().getIcon());
            icon.setFitHeight(50);
            icon.setPreserveRatio(true);

            Label title = new Label(c.getTitle().toUpperCase());
            Label skill = new Label(c.getSkill().getName().toUpperCase());
            Label type = new Label(c.getType());
            //Label rate = new Label("" + c.getRate());
            title.getStyleClass().add("quizztitle");
            // rate.getStyleClass().add("quizztitle");

            Region r = new Region();
            r.setPrefWidth(30);
            r.setPrefHeight(10);
            r.setMinWidth(10);

            quizzHBox.setHgrow(r, Priority.SOMETIMES);

            quizzHBox.getChildren().addAll(icon, title, skill, type, /*rate,*/ r, showChapterButton);

            quizzVBox.getChildren().add(quizzHBox);
        });

        return quizzVBox;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chapterNameLabel.setText(chapter.getTitle());
        if (chapter.getAvailable() == 1) {
            availableChapterCheckBox.setSelected(true);
        } else if (chapter.getAvailable() == 0) {
            availableChapterCheckBox.setSelected(false);
        }
        quizzScrollPane.setContent(createQuizzVBOX());
        confirmChapiterEditButton.setOnAction((ActionEvent e) -> {

            chapter.setContent(chapiterContentTextArea.getText());
            if (availableChapterCheckBox.isSelected() == true) {
                chapter.setAvailable(1);
            } else if (availableChapterCheckBox.isSelected() == false) {
                chapter.setAvailable(0);
            }
            App.factory.getChapiter().update(chapter);
        });

    }

}
