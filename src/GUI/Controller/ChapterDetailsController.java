/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Chapter;
import Entity.Course;
import Entity.Quizz;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.ScaleTransition;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Skander Fekih
 */
public class ChapterDetailsController implements Initializable {

    @FXML
    private ScrollPane courseDetailsScrollPane;
    @FXML
    private GridPane courseDetailsGridPane;
    @FXML
    private HBox topHbox;
    @FXML
    private Label titleCourseLabel;
    @FXML
    private Label publishedCourseLabel;
    @FXML
    private JFXButton coursebyButton;
    @FXML
    private ImageView courseInstructorAvatar;
    @FXML
    private TitledPane chaptersTitlePane;
    @FXML
    private ImageView chaptersIconImageView;
    @FXML
    private ScrollPane quizzScrollPane;
    @FXML
    private Label descriptionCourseLabel;
    @FXML
    private JFXTextArea commentCourseTextArea;
    @FXML
    private JFXButton submitCommentButton;
    @FXML
    private HBox awardsListHbox;
    @FXML
    private JFXButton subscribeToCourseButton;
    @FXML
    private WebView videoWebView;
    @FXML
    private JFXButton passQuizzBtn;

    @FXML
    private VBox quizzListVbox;
    private HomeController parent;
    private Chapter chapter;
    private Course course;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    private ObservableList<Quizz> quizzList;

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    private ObservableList<Quizz> getQuizzList() {
        return App.factory.getQuizz().findBy("CHAPTER", chapter.getId());
    }

    public void loadPassQuizz() {
        PassQuizzInterfaceController cdc = new PassQuizzInterfaceController();
        URL location = PassQuizzInterfaceController.class.getResource("/GUI/passQuizzInterface.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        cdc.setParent(this.getParent());

        fxmlLoader.setController(cdc);
        Parent root;
        try {
            root = fxmlLoader.load();
            // parent.getCentralScrollPane().getContent().
            parent.getCentralScrollPane().setContent(root);
            
        } catch (IOException ex) {
            Logger.getLogger(PassQuizzInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private VBox createQuizzVBOX() {

        quizzList = getQuizzList();
        VBox quizzVBox = new VBox();

        quizzList.stream().forEach((Quizz c) -> {
            HBox quizzHBox = new HBox(10);
            quizzHBox.getStyleClass().add("quizzhbox");
            quizzHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

            JFXButton showChapterButton = new JFXButton("Show");
            showChapterButton.getStyleClass().add("hboxbutton");
            showChapterButton.setAlignment(Pos.CENTER);
            showChapterButton.setOnAction((ActionEvent e) -> {
                System.out.println("show me this quizzz !! " + c.toString());
            });

            ImageView icon = new ImageView(c.getSkill().getIcon());
            icon.setFitHeight(50);
            icon.setPreserveRatio(true);

            Label title = new Label(c.getTitle().toUpperCase());
            Label skill = new Label(c.getSkill().getName().toUpperCase());
            Label type = new Label(c.getType());
            //Label rate = new Label("" + c.getRate());
            title.getStyleClass().add("quizztitle");
            // rate.getStyleClass().add("quizztitle");

            Region r = new Region();
            r.setPrefWidth(30);
            r.setPrefHeight(10);
            r.setMinWidth(10);

            quizzHBox.setHgrow(r, Priority.SOMETIMES);
            System.out.println("quizz ==> " + c.getTitle());
            quizzHBox.getChildren().addAll(icon, title, skill, type, /*rate,*/ r, showChapterButton);
            quizzHBox.setOnMouseClicked((MouseEvent e) -> {
                System.out.println("load quizz interface");
                loadPassQuizz();
            });

            quizzVBox.getChildren().add(quizzHBox);
        });

        return quizzVBox;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        chaptersTitlePane.setContent(createQuizzVBOX());
        quizzScrollPane.setContent(createQuizzVBOX());
        titleCourseLabel.setText(chapter.getTitle());
        coursebyButton.setText("By " + course.getInstructor().getFirstName());
        courseInstructorAvatar.setImage(new Image(course.getInstructor().getAvatar().getPath()));
        videoWebView.getEngine().load(chapter.getVideo());
        descriptionCourseLabel.setText(chapter.getDescription());
        passQuizzBtn.setOnMouseClicked((MouseEvent e) -> {
            loadPassQuizz();
        });
    }

}
