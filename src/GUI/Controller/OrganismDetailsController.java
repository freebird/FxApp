/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Instructor;
import Entity.Organism;
import Entity.Request;
import com.jfoenix.controls.JFXButton;
import fxapp.App;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class OrganismDetailsController implements Initializable {

    @FXML
    private ScrollPane organismDetailsScrollPane;
    @FXML
    private GridPane organismDetailsGridPane;
    @FXML
    private HBox topHbox;
    @FXML
    private Label organismEmailLabel;
    @FXML
    private Label organismActivityLabel;
    @FXML
    private JFXButton organismNameButton;
    @FXML
    private ImageView instructorAvatarImage;
    @FXML
    private TitledPane listInstructorsTitlePane;
    @FXML
    private TableView<?> listInstructorsTableView;
    @FXML
    private TableColumn<?, ?> instructorIdColumn;
    @FXML
    private TableColumn<?, ?> instructorFisrtNameColumn;
    @FXML
    private TableColumn<?, ?> instructoSpecialtyColumn;
    @FXML
    private TableColumn<?, ?> instructorExperienceColumn;
    @FXML
    private Label descriptionContentLabel;
    @FXML
    private VBox afilliationVBox;
    @FXML
    private HTMLEditor requestContentHTMLEditor;
    @FXML
    private JFXButton subscribeToOrganismButton;
    @FXML
    private TitledPane statTitledPane;
    @FXML
    private ImageView statImageView;
    @FXML
    private PieChart statPieChart;
    @FXML
    private VBox myInstructorsVbox;

    private HomeController parent;
    private Organism organism;
    private ObservableList<Instructor> instructorList;
    private ObservableList<Request> requestList;

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public Organism getOrganism() {
        return organism;
    }

    public void setOrganism(Organism organism) {
        this.organism = organism;
        this.organism.setRequests(App.factory.getRequest().findBy("ORGANISM", organism.getId()));
    }

    public ObservableList<Instructor> getInstructorList() {
        ObservableList<Instructor> list = FXCollections.observableArrayList();
        // my affiliated org are whom have requests type 1 from Org to Me and  status = 1 
        requestList.stream().forEach((r) -> {
            if (r.getOrganism().getId() == this.organism.getId() && (r.getStatus() == 1) && (r.getStatus() == 1)) {
                list.add(App.factory.getInsctructor().findById(r.getInstructor().getId()));
            }
        });

        return list;
    }

    public void setInstructorList(ObservableList<Instructor> instructorList) {
        this.instructorList = instructorList;
    }

    private VBox createInstructorsVBOX() {
        instructorList = this.getInstructorList();
        VBox instructorsVBox = new VBox();

        instructorList.stream().forEach((i) -> {
            HBox instructorHBox = new HBox(10);
            instructorHBox.getStyleClass().add("coursehbox");
            instructorHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            JFXButton showInstructorButton = new JFXButton("Show");
            showInstructorButton.getStyleClass().add("hboxbutton");
            showInstructorButton.setAlignment(Pos.CENTER);
            showInstructorButton.setOnAction((ActionEvent e) -> {
                // we have to show The Course
                throw new UnsupportedOperationException("Not supported yet.");

            });

            //ImageView icon = new ImageView(c.getIcon().getPath());
            //icon.setFitHeight(50);
            Label title = new Label(i.getUsername().toUpperCase());

            //Label subscribersLabel = new Label("" + c.get.size());
            //icon.setPreserveRatio(true);
            Label rate = new Label(" " + i.getRate());
            title.getStyleClass().add("coursetitle");
            rate.getStyleClass().add("coursetitle");
            Region r = new Region();
            r.setPrefWidth(30);
            r.setPrefHeight(10);
            r.setMinWidth(30);

            instructorHBox.setHgrow(r, Priority.SOMETIMES);
            instructorHBox.getChildren().addAll(/*icon,*/title, rate, r, showInstructorButton);

            instructorsVBox.getChildren().add(instructorHBox);
        });

        return instructorsVBox;
    }

    public ObservableList<Request> getRequestList() {

        return App.factory.getRequest().findBy("ORGANISM", this.organism.getId());

    }

    public void initEverything() {

        descriptionContentLabel.setText(organism.getDescription());
        organismEmailLabel.setText(organism.getEmail());
        organismActivityLabel.setText(organism.getActivity());
        organismNameButton.setText(organism.getName());
        afilliationVBox.setVisible(false);
        if (App.user.getId() == organism.getId()) {

            afilliationVBox.setVisible(true);

        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        initEverything();
        requestList = FXCollections.observableArrayList();
        instructorList = FXCollections.observableArrayList();

        myInstructorsVbox = createInstructorsVBOX();

        listInstructorsTitlePane.setContent(myInstructorsVbox);

        //listInstructorsTitlePane.getStylesheets().add("/GUI/css/style.css");
        //listInstructorsTitlePane.setContent(myInstructorsVbox);
    }

}
