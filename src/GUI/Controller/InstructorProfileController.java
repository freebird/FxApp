/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Course;
import Entity.Instructor;
import Entity.Organism;
import Entity.Request;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class InstructorProfileController implements Initializable {

    @FXML
    private ScrollPane instructorDetailsScrollPane;
    @FXML
    private GridPane instructorDetailsGridPane;
    @FXML
    private JFXButton editProfileButton;
    @FXML
    private JFXButton avatarProfileButton;
    @FXML
    private Circle avatarCircle;
    @FXML
    private JFXTextField firstNameTextField;
    @FXML
    private JFXTextField lastNameTextField;
    @FXML
    private JFXTextField phoneTextField;
    @FXML
    private JFXPasswordField passwordPasswordtField;
    @FXML
    private JFXTextField specialtyTextField;
    @FXML
    private JFXSlider expercianceSlider;
    @FXML
    private Label expercienceLabel;
    @FXML
    private JFXButton confirmEditProfileButton;
    @FXML
    private JFXButton followProfileButton;
    @FXML
    private JFXButton sendRequestProfileButton;
    @FXML
    private TitledPane organismListTitlePane;
    @FXML
    private ImageView organismIconImageView;
    @FXML
    private JFXButton subscribeToOrganismButton;
    @FXML
    private TitledPane mycoursesTitlePane;
    @FXML
    private VBox myCoursesVbox;
    @FXML
    private ImageView myCoursesIconImageView;
    @FXML
    private TitledPane statTitledPane;
    @FXML
    private ImageView statImageView;
    @FXML
    private TitledPane organismScrollPane;
    @FXML
    private ScrollPane statScrollPane;
    @FXML
    private ScrollPane organismListScrollPane;
    @FXML
    private HBox passHBox;
    @FXML
    private VBox personalInfoVbox;

    private HomeController parent;
    private Instructor instructor;
    private ObservableList<Course> courseList;
    private ObservableList<Organism> organismList;
    private ObservableList<Request> requestList;

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
        this.instructor.setCourses(App.factory.getCourse().findBy("INSTRUCTOR", instructor.getId()));
        this.instructor.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", instructor.getId()));
        //this.requestList = App.factory.getRequest().findBy("INSTRUCTOR", this.instructor.getId());
    }

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public ObservableList<Course> getCourseList() {
        ObservableList<Course> list = FXCollections.observableArrayList();

        list.addAll(App.factory.getCourse().findBy("INSTRUCTOR", this.instructor.getId()));

        return list;
    }

    public ObservableList<Organism> getOrganismList() {
        ObservableList<Organism> list = FXCollections.observableArrayList();
        requestList = App.factory.getRequest().findBy("INSTRUCTOR=" + instructor.getId() + " AND STATUS", 1);
        requestList.stream().forEach((r) -> {
            System.out.println("foreach r org ===> " + r.getOrganism().getId());
            list.add(App.factory.getOrganism().findById(r.getOrganism().getId()));

        });

        return list;
    }

    public void loadCourseDetails(Course course) {

        CourseDetailsController cdc = new CourseDetailsController();
        URL location = InstructorProfileController.class.getResource("/GUI/courseDetails.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        cdc.setParent(this.getParent());

        cdc.setCours(App.factory.getCourse().findById(course.getId()));
        fxmlLoader.setController(cdc);
        Parent root;
        try {
            root = fxmlLoader.load();
            // parent.getCentralScrollPane().getContent().
            parent.getCentralScrollPane().setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(CourseDetailsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void initProfile() {
        mycoursesTitlePane.getStylesheets().add("/GUI/css/style.css");
        myCoursesVbox = new VBox();
        avatarProfileButton.setVisible(false);
        firstNameTextField.setDisable(true);
        lastNameTextField.setDisable(true);
        avatarProfileButton.setDisable(true);
        phoneTextField.setDisable(true);
        specialtyTextField.setDisable(true);
        expercianceSlider.setDisable(false);

        firstNameTextField.setText(instructor.getFirstName());
        lastNameTextField.setText(instructor.getLastName());
        expercianceSlider.setValue(instructor.getExperience());
        expercienceLabel.setText("" + instructor.getExperience());
        specialtyTextField.setText((instructor.getSpeciality()));
        phoneTextField.setText(instructor.getPhone());
        avatarCircle.setFill(new ImagePattern(new Image(instructor.getAvatar().getPath())));
        sendRequestProfileButton.setVisible(false);
        followProfileButton.setVisible(false);

        if (App.user.getId() == instructor.getId()) {
            confirmEditProfileButton.setVisible(false);
            editProfileButton.setVisible(true);
            sendRequestProfileButton.setVisible(false);

        } else {
            editProfileButton.setVisible(false);
            avatarProfileButton.setVisible(true);
            if (App.user.getRoles().contains("STUDENT") && App.factory.getSubscription().findOneBy("STUDENT =" + App.user.getId() + " AND INSTRUCTOR =" + instructor.getId() + " AND TYPE", "FOLLOW") == null) {

                followProfileButton.setVisible(true);

            }
        }

        if (App.user.getRoles().contains("ORGANISM") && App.factory.getRequest().findOneBy("ORGANISM =" + App.user.getId() + " AND INSTRUCTOR ", instructor.getId()) == null) {

            sendRequestProfileButton.setVisible(true);

        }

    }

    private VBox createCoursesVBOX() {
        courseList = this.getCourseList();
        VBox coursesVBox = new VBox(5);
        coursesVBox.getStyleClass().add("my-courses-vbox");

        courseList.stream().forEach((c) -> {
            HBox courseHBox = new HBox(10);
            courseHBox.getStyleClass().add("coursehbox");
            courseHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            JFXButton showCourseButton = new JFXButton("Show");
            showCourseButton.getStyleClass().add("jfx-button");
            showCourseButton.setAlignment(Pos.CENTER);
            showCourseButton.setOnAction((ActionEvent e) -> {
                loadCourseDetails(c);
                System.out.println("Show me this course !!!" + c.toString());
                //load Course Details in Home.fxml  setCenter ;)  tsarref kifech touslelha :3 :3 

            });

            ImageView icon = new ImageView(c.getIcon().getPath());
            icon.setFitHeight(57);
            Label title = new Label(c.getTitle().toUpperCase());

            Label subscribersLabel = new Label("" + c.getSubscription().size());
            icon.setPreserveRatio(true);
            Label rate = new Label(" " + c.getRate());
            title.getStyleClass().add("long-hbox-label");
            rate.getStyleClass().add("long-hbox-label");
            subscribersLabel.getStyleClass().add("long-hbox-label");
            Region r = new Region();
            r.setPrefWidth(30);

            r.setMinWidth(30);

            HBox.setHgrow(r, Priority.SOMETIMES);
            courseHBox.getChildren().addAll(icon, title, subscribersLabel, rate, r, showCourseButton);

            coursesVBox.getChildren().add(courseHBox);
        });

        return coursesVBox;
    }

    private HBox createOrganismHBox() {
        organismList = getOrganismList();
        HBox hbox = new HBox(10);

        organismList.stream().forEach((a) -> {
            VBox vbox = new VBox(5);
            Label name = new Label(a.getName());
            ImageView img = new ImageView(a.getAvatar().getPath());
            img.setFitHeight(130);
            img.setFitWidth(125);
            name.contentDisplayProperty().set(ContentDisplay.TOP);
            vbox.getChildren().addAll(img, name);
            vbox.setOnMouseClicked((MouseEvent event) -> {
                System.out.println("show me this org !!" + a.toString());
            });
            hbox.getChildren().add(vbox);
        });

        return hbox;
    }

    public ObservableList<Request> getRequestList() {

        return App.factory.getRequest().findBy("INSTRUCTOR", this.instructor.getId());

    }

    public ObservableList<Course> getMyTopCourses() {

        return App.factory.getCourse().findMyTopCourses(instructor.getId());

    }

    private VBox createStatVBox() {

        VBox box = new VBox();

        getMyTopCourses().stream().forEach((Course m) -> {
            HBox top = new HBox();
            HBox bottom = new HBox();

            ImageView img = new ImageView(m.getIcon().getPath());
            img.setFitHeight(45);
            img.setPreserveRatio(true);
            img.setSmooth(true);
            img.setStyle("-fx-border-raius:50%");

            Label name = new Label(m.getTitle());
            name.setGraphic(img);
            name.contentDisplayProperty().set(ContentDisplay.LEFT);
            name.setGraphicTextGap(20);
            Label value = new Label("" + ((m.getRate() / 20) * 100) + "%");
            Region r = new Region();
            r.setPrefWidth(400);
            r.setMaxWidth(Double.MAX_VALUE);
            HBox.setHgrow(r, Priority.ALWAYS);
            top.getChildren().addAll(name, r, value);

            ProgressBar pb = new ProgressBar((m.getRate() / 20));
            Timeline task = new Timeline(
                    new KeyFrame(
                            Duration.ZERO,
                            new KeyValue(pb.progressProperty(), 0)
                    ),
                    new KeyFrame(
                            Duration.seconds(3),
                            new KeyValue(pb.progressProperty(), m.getRate() / 20)
                    )
            );

            HBox.setHgrow(pb, Priority.ALWAYS);
            pb.setMaxWidth(Double.MAX_VALUE);
            bottom.getChildren().add(pb);
            bottom.setMaxWidth(Double.MAX_VALUE);
            bottom.setMaxHeight(15.0);

            HBox.setHgrow(top, Priority.ALWAYS);
            HBox.setHgrow(bottom, Priority.ALWAYS);
            box.getChildren().addAll(top, bottom);
            box.setMinHeight(70);
            task.playFromStart();
        });

        return box;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ////////////////////////////////////////////////////////////////
        courseList = FXCollections.observableArrayList();
        requestList = FXCollections.observableArrayList();
        organismList = FXCollections.observableArrayList();
        organismListScrollPane.setContent(createOrganismHBox());
        mycoursesTitlePane.getStylesheets().add("/GUI/css/style.css");
        mycoursesTitlePane.setContent(createCoursesVBOX());
        statScrollPane.setContent(createStatVBox());
        initProfile();

        ///////////////////////////////////////////////////////////////
        editProfileButton.setOnAction((ActionEvent e) -> {
            avatarProfileButton.setDisable(false);
            phoneTextField.setDisable(false);

            firstNameTextField.setDisable(false);
            lastNameTextField.setDisable(false);
            //   passwordPasswordtField.setVisible(true);
            expercianceSlider.setDisable(false);
            confirmEditProfileButton.setVisible(true);
            specialtyTextField.setDisable(false);
            avatarProfileButton.setVisible(true);

        });

        ///////////////////////////////////////////////////////////////////
        confirmEditProfileButton.setOnAction((ActionEvent e) -> {
            instructor.setFirstName(firstNameTextField.getText());
            instructor.setLastName(lastNameTextField.getText());
            //  instructor.setPassword(passwordPasswordtField.getText());
            instructor.setExperience((int) expercianceSlider.getValue());
            instructor.setSpeciality(specialtyTextField.getText());

            App.factory.getInsctructor().update(instructor);
            initProfile();

        });

        ///////////////////////////////////////////////////////////////////////
        expercianceSlider.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                expercienceLabel.setText("" + expercianceSlider.getValue()); //To change body of generated methods, choose Tools | Templates.
            }
        });
        ///////////////////////////////////////////////////////////////////////

    }

}
