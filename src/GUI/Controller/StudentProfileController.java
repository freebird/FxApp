/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Award;
import Entity.Course;
import Entity.Instructor;
import Entity.Mark;
import Entity.Student;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class StudentProfileController implements Initializable {

    @FXML
    private ScrollPane studentProfileScrollPane;
    @FXML
    private GridPane studentProfileGridPane;
    @FXML
    private TitledPane myCoursesTitledPane;
    @FXML
    private ImageView myCoursesImageView;
    @FXML
    private ScrollPane myCoursesScrollPane;

    @FXML
    private TableColumn<Course, String> coursesIdColumn;
    @FXML
    private TableColumn<Course, String> coursesTitleColumn;
    @FXML
    private ImageView coursesTitleImageView;
    @FXML
    private TableColumn<Course, String> coursesCategoryColumn;
    @FXML
    private ImageView coursesCategoryImageView;
    @FXML
    private TableColumn<Course, Instructor> coursesInstructorColumn;
    @FXML
    private ImageView coursesInstructorImageView;
    @FXML
    private TableColumn<Course, String> coursesMarkColumn;
    @FXML
    private ImageView coursesMarkImageView;
    @FXML
    private JFXButton editButton;
    @FXML
    private JFXButton avatrButton;
    @FXML
    private Circle avatarCircle;
    @FXML
    private JFXTextField firstNameTextField;
    @FXML
    private JFXTextField lastNameTextField;
    @FXML
    private JFXPasswordField passwordPasswordField;
    @FXML
    private JFXButton confirmButton;
    @FXML
    private TitledPane myAwardsTitledPane;
    @FXML
    private ImageView myAwardsImageView;
    @FXML
    private ScrollPane myAwardsScrollPane;
    @FXML
    private ScrollPane statScrollPane;
    @FXML
    private HBox myAwardsHBox;
    @FXML
    private TitledPane statTitledPane;
    @FXML
    private ImageView statImageView;
    @FXML
    private BarChart<?, ?> statBarChart;

    /**
     * belguith
     */
    private HomeController parent;

    private Student student;
    private ObservableList<Course> courseList;
    private ObservableList<Award> awardList;
    private ObservableList<Mark> markList;

    public HomeController getParent() {
        return parent;
    }

    public void setParent(HomeController parent) {
        this.parent = parent;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
        this.student.setSubcriptions(App.factory.getSubscription().findBy("STUDENT", student.getId()));
    }

    public ObservableList<Course> getCourseList() {
        ObservableList<Course> list = FXCollections.observableArrayList();
        student.getSubcriptions().stream().forEach((s) -> {
            list.add(App.factory.getCourse().findById(s.getCourse().getId()));
        });
        return list;
    }

    public ObservableList<Award> getAwardList() {

        ObservableList<Award> list = FXCollections.observableArrayList();

        App.factory.getAward().findBy("STUDENT", student.getId()).stream().forEach((a) -> {
            list.add(a);
        });
        return list;

    }

    public ObservableList<Mark> getMarkList() {

        ObservableList<Mark> list = FXCollections.observableArrayList();

        App.factory.getMarkDAO().findTopMarks(student.getId()).stream().forEach((m) -> {
            list.add(m);
        });
        return list;

    }

    public void setCourseList(ObservableList<Course> courseList) {
        this.courseList = courseList;
    }

    private void initProfile() {
        myCoursesTitledPane.getStylesheets().add("/GUI/css/style.css");
        if (App.user.getId() == student.getId()) {
            confirmButton.setVisible(false);
            editButton.setVisible(true);

        } else {
            editButton.setVisible(false);

        }
        firstNameTextField.setDisable(true);
        lastNameTextField.setDisable(true);
        avatrButton.setVisible(false);

    }

    private void loadProfileContent() {

        firstNameTextField.setText(student.getFirstName());
        lastNameTextField.setText(student.getLastName());
        if (student.getAvatar().getPath() != null) {
            Image avatar = new Image(student.getAvatar().getPath());
            avatarCircle.setFill(new ImagePattern(avatar));
        }

    }

        public void loadCourseDetails(Course course) {

        CourseDetailsController cdc = new CourseDetailsController();
        URL location = InstructorProfileController.class.getResource("/GUI/courseDetails.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        cdc.setParent(this.getParent());

        cdc.setCours(App.factory.getCourse().findById(course.getId()));
        fxmlLoader.setController(cdc);
        Parent root;
        try {
            root = fxmlLoader.load();
            // parent.getCentralScrollPane().getContent().
            parent.getCentralScrollPane().setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(CourseDetailsController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private VBox createCoursesVBOX() {
        courseList = this.getCourseList();
        VBox coursesVBox = new VBox(5);
        coursesVBox.getStyleClass().add("my-courses-vbox");
        courseList.stream().forEach((c) -> {
            HBox courseHBox = new HBox(10);
            courseHBox.getStyleClass().add("coursehbox");
            courseHBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);

            JFXButton showCourseButton = new JFXButton("Show");
            showCourseButton.getStyleClass().add("jfx-button");
            showCourseButton.setAlignment(Pos.CENTER);
            showCourseButton.setOnAction((ActionEvent e) -> {
                loadCourseDetails(c);
                System.out.println("show me this course now ");

            });

            ImageView icon = new ImageView(c.getIcon().getPath());
            icon.setFitHeight(57);
            icon.setPreserveRatio(true);

            Label title = new Label(c.getTitle().toUpperCase());
            //title.getStyleClass().add("long-hbox-label");
            title.setPrefWidth(Double.MIN_VALUE);
            title.setMinWidth(120);
            title.setWrapText(false);

            Label instructorLabel = new Label("By : " + c.getInstructor().getUsername().toUpperCase());
            //instructorLabel.getStyleClass().add("long-hbox-label");
            instructorLabel.setPrefWidth(Double.MIN_VALUE);

            instructorLabel.setMinWidth(120);
            instructorLabel.setWrapText(false);
            Label rate = new Label("" + c.getRate());
            //rate.getStyleClass().add("long-hbox-label");
            rate.setMinWidth(40);
            rate.setWrapText(false);

            Region r = new Region();
            r.setPrefWidth(40);
            r.setMinWidth(40);

            courseHBox.getChildren().addAll(icon, title, instructorLabel, rate, r, showCourseButton);

            coursesVBox.getChildren().add(courseHBox);
        });

        return coursesVBox;
    }

    private HBox createAwardsHBox() {
        System.out.println("load step 2 AWARDS HBOX");

        awardList = getAwardList();
        HBox box = new HBox(10);
        awardList.stream().forEach((a) -> {
            ImageView img = new ImageView(a.getSkill().getIcon());
            img.setFitHeight(80);
            img.setPreserveRatio(true);
            img.setSmooth(true);
            Label name = new Label(a.getSkill().getName(), img);
            name.setPrefWidth(Double.MIN_VALUE);
            name.setMinWidth(120);
            name.setWrapText(false);
            System.out.println("AWAD ICON = " + a.getSkill().getIcon());
            name.contentDisplayProperty().set(ContentDisplay.TOP);
            box.getChildren().add(name);
        });

        return box;
    }

    private VBox createStatVBox() {
        System.out.println("load step 2 STAT VBOX");

        markList = getMarkList();
        VBox box = new VBox();
        markList.stream().forEach((m) -> {
            VBox skillVBox = new VBox();
            HBox top = new HBox();
            skillVBox.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            ImageView img = new ImageView(m.getQuizz().getSkill().getIcon());
            img.setFitHeight(50);
            img.setPreserveRatio(true);
            img.setSmooth(true);

            Label name = new Label(m.getQuizz().getSkill().getName());

            Label value = new Label("" + ((m.getValue() / 20) * 100) + "%");
            System.out.println("Mark stat = " + m.getQuizz().getSkill().getIcon());
            name.contentDisplayProperty().set(ContentDisplay.TOP);
            Region r = new Region();
            r.setPrefWidth(400);
            top.setHgrow(r, Priority.ALWAYS);
            ProgressBar pb = new ProgressBar((m.getValue() / 20));
            Timeline task = new Timeline(
                    new KeyFrame(
                            Duration.ZERO,
                            new KeyValue(pb.progressProperty(), 0)
                    ),
                    new KeyFrame(
                            Duration.seconds(3),
                            new KeyValue(pb.progressProperty(), m.getValue() / 20)
                    )
            );
            pb.setMaxWidth(Double.MAX_VALUE);

            top.getChildren().addAll(img, name, r, value);
            skillVBox.getChildren().addAll(top, pb);

            box.getChildren().add(skillVBox);
            task.playFromStart();
        });

        return box;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        initProfile();
        loadProfileContent();
        myCoursesTitledPane.setContent(createCoursesVBOX());
        //myAwardsHBox = new HBox();
        myAwardsScrollPane.setContent(createAwardsHBox());
        //myAwardsHBox = createAwardsHBox(); //// Waiting For AawardDAO
        //myAwardsHBox = new HBox();

        statScrollPane.setContent(createStatVBox());
        statTitledPane.setContent(statScrollPane);
        //myAwardsHBox = createAwardsHBox(); //// Waiting For AawardDAO

        editButton.setOnAction((ActionEvent e) -> {
            firstNameTextField.setDisable(false);
            lastNameTextField.setDisable(false);

            avatrButton.setVisible(true);
            confirmButton.setVisible(true);
            confirmButton.setDisable(false);
            //clear the Borderpane's center
            //Load Course Interface by passin Course as parameter
        });

    }

}
