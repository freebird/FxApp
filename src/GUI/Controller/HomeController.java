/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import Entity.Instructor;
import Entity.Student;
import Entity.User;
import com.jfoenix.controls.JFXButton;
import fxapp.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

/**
 * FXML Controller class
 *
 * @author Belguith
 */
public class HomeController implements Initializable {

    @FXML
    private BorderPane homeBorderPane;
    @FXML
    private VBox leftBar;
    @FXML
    private Circle avatarCircle;
    @FXML
    private JFXButton userFirstLastNameLabel;
    @FXML
    private JFXButton homeButton;
    @FXML
    private JFXButton userNotification1Label;
    @FXML
    private JFXButton userNotification2Label;
    @FXML
    private JFXButton reduceButton;
    @FXML
    private ScrollPane centralScrollPane;

    public ScrollPane getCentralScrollPane() {
        return centralScrollPane;
    }

    public void setCentralScrollPane(ScrollPane centralScrollPane) {
        this.centralScrollPane = centralScrollPane;
    }

    public void loadStudentSideBar() {
        Image avatar = null;
        try {
            avatar = new Image(App.user.getAvatar().getPath());
            Thread.sleep(100);
            avatarCircle.setFill(new ImagePattern(avatar));

        } catch (InterruptedException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        userFirstLastNameLabel.setText(App.user.getUsername());

        Student s = App.factory.getStudent().findById(App.user.getId());
        App.user = s;
        System.out.println("App.user side bar student ==> " + App.user.getUsername());
        userNotification1Label.setVisible(true);
        userNotification2Label.setVisible(true);

    }

    public void loadInstructorSideBar() {
        Image avatar = null;
        try {
            avatar = new Image(App.user.getAvatar().getPath());
            Thread.sleep(100);
            avatarCircle.setFill(new ImagePattern(avatar));
            System.out.println("Avatar OK in loadsideBar");
        } catch (InterruptedException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        userFirstLastNameLabel.setText(App.user.getUsername());
        Instructor s = App.factory.getInsctructor().findById(App.user.getId());
        System.out.println("S ===> " + s.getUsername());
    }

    public void loadOrganismSideBar() {
        Image avatar = null;
        try {
            avatar = new Image(App.user.getAvatar().getPath());
            Thread.sleep(100);
            avatarCircle.setFill(new ImagePattern(avatar));

        } catch (InterruptedException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        userFirstLastNameLabel.setText(App.user.getUsername());
    }

    public void loadStudentProfile(User user) {

        StudentProfileController spc = new StudentProfileController();
        URL location = StudentProfileController.class.getResource("/GUI/studentProfile.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        spc.setParent(this);
        spc.setStudent(App.factory.getStudent().findById(user.getId()));
        fxmlLoader.setController(spc);
        Parent root;
        try {
            System.out.println("loading profile.fxml" + user.getUsername());
            root = fxmlLoader.load();
            centralScrollPane.setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(StudentProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadHome(User user) {

        BrowseCoursesController spc = new BrowseCoursesController();
        URL location = StudentProfileController.class.getResource("/GUI/browseCourses.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        spc.setParent(this);

        if (App.user.getRoles().contains("STUDENT")) {
            spc.setStudent(App.factory.getStudent().findById(user.getId()));

        } else if (App.user.getRoles().contains("INSTRUCTOR")) {
            spc.setInstructor(App.factory.getInsctructor().findById(user.getId()));

        } else if (App.user.getRoles().contains("ORGANISM")) {
            spc.setOrganism(App.factory.getOrganism().findById(user.getId()));
        }

        fxmlLoader.setController(spc);
        Parent root;
        try {
            root = fxmlLoader.load();
            centralScrollPane.setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(StudentProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadInstructorProfile(User user) {
        InstructorProfileController spc = new InstructorProfileController();
        URL location = InstructorProfileController.class.getResource("/GUI/instructorProfile.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        spc.setParent(this);

        spc.setInstructor(App.factory.getInsctructor().findById(user.getId()));
        fxmlLoader.setController(spc);
        Parent root;
        try {
            System.out.println("Loading Instructor Profile");
            root = fxmlLoader.load();
            centralScrollPane.setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(InstructorProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadOrganismProfile(User user) {
        OrganismDetailsController spc = new OrganismDetailsController();
        URL location = OrganismDetailsController.class.getResource("/GUI/organismDetails.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        spc.setParent(this);

        spc.setOrganism(App.factory.getOrganism().findById(user.getId()));
        fxmlLoader.setController(spc);
        Parent root;
        try {
            root = fxmlLoader.load();
            centralScrollPane.setContent(root);
        } catch (IOException ex) {
            Logger.getLogger(OrganismDetailsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        if (App.user.getRoles().contains("STUDENT")) {
            loadStudentSideBar();
            System.out.println("side bar loaded");
            loadStudentProfile(App.user);
            userFirstLastNameLabel.setOnAction((ActionEvent e) -> {
                loadStudentProfile(App.user);
            });

        } else if (App.user.getRoles().contains("INSTRUCTOR")) {
            System.out.println("INSTRUCTOR SIDE BAR LOADING ");
            loadInstructorSideBar();
            System.out.println("side bar loaded");
            loadInstructorProfile(App.user);
            userFirstLastNameLabel.setOnAction((ActionEvent e) -> {
                loadInstructorProfile(App.user);

            });
        } else if (App.user.getRoles().contains("ORGANISM")) {
            loadOrganismSideBar();
            System.out.println("side bar loaded");
            loadOrganismProfile(App.user);
            userFirstLastNameLabel.setOnAction((ActionEvent e) -> {

                loadOrganismProfile(App.user);

            });
        }

        homeButton.setOnAction((ActionEvent e) -> {
            System.out.println("go to my Home");
            System.out.println("logged user " + App.user.getUsername());
            //loadStudentProfile(App.user);
            loadHome(App.user);

        });

    }

}
