/**
 * *********************************************************************
 * Module: Instructor.java Author: Belguith Purpose: Defines the Class
 * Instructor
 * *********************************************************************
 */
package Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Instructor extends User {

    private int experience;

    private String speciality;

    private String firstName;

    private String lastName;

    private String phone;

    private float rate;

    private ObservableList<Request> requests;

    private ObservableList<Course> courses;

    public Instructor() {
        super();
        this.avatar = new Media();
        this.requests = FXCollections.observableArrayList();
        this.courses = FXCollections.observableArrayList();
    }

    public Instructor(int id, String phone, String firstName, String lastName, String email, String password, int experience, String speciality) {
        super();
        this.id = id;
        this.email = email;
        this.password = password;
        this.experience = experience;
        this.speciality = speciality;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.avatar = new Media();
        this.requests = FXCollections.observableArrayList();
        this.courses = FXCollections.observableArrayList();

    }

    public ObservableList<Course> getCourses() {
        return courses;
    }

    public void setCourses(ObservableList<Course> courses) {
        this.courses = courses;
    }

    public void addCourse(Course r) {
        this.courses.add(r);

    }

    public ObservableList<Request> getRequests() {
        return requests;
    }

    public void setRequests(ObservableList<Request> requests) {
        this.requests = requests;
    }

    public void addRequest(Request r) {
        requests.add(r);

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String newFirstName) {
        firstName = newFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String newLastName) {
        lastName = newLastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int newExperience) {
        experience = newExperience;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String newSpeciality) {
        speciality = newSpeciality;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "\n Instructor{" + "experience=" + experience + ", speciality=" + speciality + ", firstName=" + firstName + ", lastName=" + lastName + "}\n";
    }

}
