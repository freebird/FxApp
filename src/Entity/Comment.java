/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Date;

/**
 *
 * @author Belguith
 */
public class Comment {

    private String id;
    private Date created;
    private Date modified;
    private String content;
    private String fullname;
    private String picture;
    private int upvote;
    private boolean hasupvote;
    private Comment parent;
    private Course course;

    public Comment() {
        this.course = new Course();
        
    }

    
    
    public Comment(String id, Date created, String content, String fullname) {
        this.id = id;
        this.created = created;
        this.content = content;
        this.fullname = fullname;
        this.parent = new Comment();
        this.course = new Course();
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getUpvote() {
        return upvote;
    }

    public void setUpvote(int upvote) {
        this.upvote = upvote;
    }

    public boolean isHasupvote() {
        return hasupvote;
    }

    public void setHasupvote(boolean hasupvote) {
        this.hasupvote = hasupvote;
    }

    public Comment getParent() {
        return parent;
    }

    public void setParent(Comment parent) {
        this.parent = parent;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Comment{" + "id=" + id + ", fullname=" + fullname + ", upvote=" + upvote + '}';
    }

}
