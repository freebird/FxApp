/**
 * *********************************************************************
 * Module: Administrator.java Author: Belguith Purpose: Defines the Class
 * Administrator
 * *********************************************************************
 */
package Entity;

public class Administrator extends User {

    private String firstName;
    private String lastName;

    public Administrator() {
    }

    public Administrator(int id, String firstName, String lastName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;

        this.email = email;
        this.password = password;
        this.avatar = new Media();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Administrator{" + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }

}
