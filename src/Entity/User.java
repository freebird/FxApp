/**
 * *********************************************************************
 * Module: User.java Author: Belguith Purpose: Defines the Class User
 * *********************************************************************
 */
package Entity;

import java.sql.Date;

public class User {

    protected int id;

    protected String username;

    protected String email;

    protected boolean enabled;

    protected boolean locked;

    protected String roles;

    protected String password;

    protected Date dateOfBirth;

    protected Media avatar;

    protected String description;

    public User() {

        this.avatar = new Media();

    }

    public User(int id, String username, String email, String roles, String description) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.description = description;
        this.avatar = new Media();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Media getAvatar() {
        return avatar;
    }

    public void setAvatar(Media avatar) {
        this.avatar = avatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", email=" + email + '}';
    }

}
