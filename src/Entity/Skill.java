/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author Skander Fekih
 */
public class Skill {

    private int id;
    private String name;
    private String icon;
    private int value;

    public Skill(int id, String name, String icon, int value) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.value = value;

    }

    public Skill() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Skill{" + "id=" + id + ", name=" + name + ", icon=" + icon + '}';
    }

}
