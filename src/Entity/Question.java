package Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Question {

    private int id;
    private int level;
    private String statement;
    private Quizz quizz;
    private ObservableList<Response> Response;
    

    public Question() {
        this.quizz = new Quizz();

    }

    public Question(int id ,int level, String statement) {
        this.id = id ;
        this.level = level;
        this.statement = statement;
        this.quizz = new Quizz();
        this.Response = FXCollections.observableArrayList();

    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public ObservableList<Response> getResponse() {
        return Response;
    }

   

    public void ajouterResponse(Response R) {
        Response.add(R);
    }

    

    public void setResponse(ObservableList<Response> ValidResponse) {
        this.Response = ValidResponse;
    }

    
    @Override
    public String toString() {
        return "\n Question{" + "id=" + id + ", level=" + level + ", statement=" + statement + "}\n";
    }

}
