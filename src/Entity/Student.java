/**
 * *********************************************************************
 * Module: Student.java Author: Belguith Purpose: Defines the Class Student
 * *********************************************************************
 */
package Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Student extends User {

    private String firstName;
    private String lastName;
    public ObservableList<Subscription> subcriptions;

    public Student() {
        super();
        this.subcriptions = FXCollections.observableArrayList();

    }

    public Student(int id, String firstName, String lastName, String email) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.subcriptions = FXCollections.observableArrayList();

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String newFirstName) {
        firstName = newFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String newLastName) {
        lastName = newLastName;
    }

    public ObservableList<Subscription> getSubcriptions() {
        return subcriptions;
    }

    public void setSubcriptions(ObservableList<Subscription> subcriptions) {
        this.subcriptions = subcriptions;
    }

    @Override
    public String toString() {
        return "Student{" + "firstName=" + firstName + ", lastName=" + lastName + '}';
    }

}
