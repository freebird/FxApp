/**
 * *********************************************************************
 * Module: Note.java Author: Belguith Purpose: Defines the Class Note
 * *********************************************************************
 */
package Entity;

public class Mark {

    private int id;
    private double value;

    private User user;
    private Quizz quizz;

    public Mark() {
    }

    public Mark(int id, double note) {
        this.id = id;
        this.value = note;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Mark{" + "id=" + id + ", note=" + value + '}';
    }

}
