/**
 * *********************************************************************
 * Module: Organism.java Author: Hiba Purpose: Defines the Class Organism
 * *********************************************************************
 */
package Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Organism extends User {

    private String name;

    private String activity;

    private String phone;

    private String fax;

    private String address;

    private ObservableList<Request> requests;

    public Organism(int id, String name, String activity, String phone, String fax, String address) {
        super();
        this.id = id;
        this.name = name;
        this.activity = activity;
        this.phone = phone;
        this.fax = fax;
        this.address = address;
        this.requests = FXCollections.observableArrayList();
    }

    public Organism() {
        super();
        this.requests = FXCollections.observableArrayList();
    }

    public ObservableList<Request> getRequests() {
        return requests;
    }

    public void setRequests(ObservableList<Request> requests) {
        this.requests = requests;
    }

    public void addRequest(Request r) {
        requests.add(r);

    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String newActivity) {
        activity = newActivity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String newPhone) {
        phone = newPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String newFax) {
        fax = newFax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String newAddress) {
        address = newAddress;
    }

    @Override
    public String toString() {
        return "Organism{" + "name=" + name + ", activity=" + activity + '}';
    }

}
