package Entity;

import java.sql.Date;

/**
 *
 * @author Belguith
 */
public class Subscription {

    private int id;
    private Student student;
    private Course course;
    private Instructor instructor;
    private Date subDate;
    private String type;

    public Subscription() {
    }

    public Subscription(int id, Date subDate, String type) {
        this.id = id;
        this.subDate = subDate;
        this.type = type;
        this.course = new Course();
        this.student = new Student();
        this.instructor = new Instructor();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Date getSubDate() {
        return subDate;
    }

    public void setSubDate(Date subDate) {
        this.subDate = subDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
