/**
 * *********************************************************************
 * Module: Quizz.java Author: hiba Purpose: Defines the Class Quizz
 * *********************************************************************
 */
package Entity;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Quizz {

    private int id;

    private String type;

    private int level;

    private Skill skill;

    private String title;

    private int duration;

    private Chapter chapter;

    private ObservableList<Question> questions;

    public Quizz() {
        questions = FXCollections.observableArrayList();
        chapter = new Chapter();
        skill = new Skill();
    }

    public Quizz(String type, int level, String title, int duration) {
        this.type = type;
        this.level = level;

        this.title = title;
        this.duration = duration;
        questions = FXCollections.observableArrayList();
        chapter = new Chapter();
        skill = new Skill();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public ObservableList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ObservableList<Question> questions) {
        this.questions = questions;
    }

    public void addQuestion(Question q) {
        questions.add(q);
    }

    @Override
    public String toString() {
        return "Quizz{" + "id=" + id + ", type=" + type + ", title=" + title + '}';
    }

}
