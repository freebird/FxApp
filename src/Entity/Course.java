/**
 * *********************************************************************
 * Module: Course.java Author: Cyrine Purpose: Defines the Class Course
 * *********************************************************************
 */
package Entity;

import java.util.Date;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Course {

    private int id;

    private String title;

    private int level;

    private Date duration;

    private String description;

    private String book;

    private int available;

    private Date published;

    private Double rate;

    public ObservableList<Subscription> subscriptions;

    private Instructor instructor;

    private Media icon;

    private Video video;

    public Course() {
        this.icon = new Media();
        this.video = new Video();
        this.instructor = new Instructor();
        this.subscriptions = FXCollections.observableArrayList();
    }

    public Course(int id, String title, int level, Date duration, String description, String book) {
        this.id = id;
        this.title = title;
        this.level = level;
        this.duration = duration;
        this.description = description;
        this.book = book;
        this.icon = new Media();
        this.video = new Video();
        this.instructor = new Instructor();
        this.subscriptions = FXCollections.observableArrayList();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public ObservableList<Subscription> getSubscription() {
        return subscriptions;
    }

    public void setSubscription(ObservableList<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public void addSubscription(Subscription s) {
        this.subscriptions.add(s);

    }

    public int hasSubscription(Subscription s) {
        int i;

        for (i = 0; i < this.subscriptions.size(); i++) {
            if ((this.subscriptions.get(i).getCourse() == s.getCourse()) && (this.subscriptions.get(i).getStudent() == s.getStudent())) {
                return i;
            }
        }
        return -1;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Media getIcon() {
        return icon;
    }

    public void setIcon(Media icon) {
        this.icon = icon;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    @Override
    public String toString() {
        return "Course{" + "id=" + id + ", title=" + title + ", level=" + level + '}';
    }

}
