package Entity;

public class Response {

    private int id;
    private Question question;
    private String statement;
    private String type;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Response() {
        this.question = new Question();
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Response(int id, String statement, String type) {
        this.id = id;
        this.statement = statement;
        this.type = type;
        this.question = new Question();

    }

    @Override
    public String toString() {
        return "Response{" + "id=" + id + ", question=" + question + '}';
    }

}
