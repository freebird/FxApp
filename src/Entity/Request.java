package Entity;

import java.sql.Date;

public class Request {

    private int id;
    private Organism organism;
    private Instructor instructor;
    private int type;
    private int status;
    private Date date;
    private String content;
    private int seen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSeen() {
        return seen;
    }

    public void setSeen(int seen) {
        this.seen = seen;
    }

    public Request(String content, int type, int status, Date date) {
        this.content = content;
        this.type = type;
        this.status = status;
        this.date = date;
        instructor = new Instructor();
        organism = new Organism();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Organism getOrganism() {
        return organism;
    }

    public void setOrganism(Organism organism) {
        this.organism = organism;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Request() {
    }

    @Override
    public String toString() {
        return "Request {" + "id=" + id + ", idOrganism=" + organism + ", idInstructor=" + instructor + ", type=" + type + ", status=" + status + ", date=" + date + '}';
    }

}
