package Entity;

public class Award {

    private int id;

    private int level;

    private Student Student;

    private Skill skill;

    private Quizz quizz;

    public Award() {
    }

    public Award(int id, int level) {
        this.id = id;
        this.level = level;
        
    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
    }

    public int getId() {
        return id;
    }

    public int getLevel() {
        return level;
    }

    public Student getStudent() {
        return Student;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setStudent(Student Student) {
        this.Student = Student;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "Award{" + "id=" + id + ", level=" + level + ", Student=" + Student + '}';
    }

}
