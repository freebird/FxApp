/**
 * *********************************************************************
 * Module: Chapiter.java Author: Belguith Purpose: Defines the Class Chapiter
 * *********************************************************************
 */
package Entity;

import javafx.collections.ObservableList;

public class Chapter {

    private int id;

    private String title;

    private String description;

    private String presentation;

    private String video;

    private String content;

    private Course course;

    private int available;

    private ObservableList<Quizz> quizz;

    public Chapter() {

    }

    public Chapter(String title, String description, String presentation, String video, String content, int available) {
        this.title = title;
        this.description = description;
        this.presentation = presentation;
        this.video = video;
        this.content = content;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public ObservableList<Quizz> getQuizz() {
        return quizz;
    }

    public void setQuizz(ObservableList<Quizz> quizz) {
        this.quizz = quizz;
    }

    public void addQuizz(Quizz s) {
        this.quizz.add(s);
    }

    public int hasQuizz(Quizz s) {
        int i;

        for (i = 0; i < this.quizz.size(); i++) {
            if ((this.quizz.get(i).getChapter() == s.getChapter())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public String toString() {
        return "Chapiter{" + "id=" + id + ", title=" + title + ", description=" + description + ", presentation=" + presentation + ", video=" + video + ", content=" + content + ", available=" + available + '}';
    }

}
