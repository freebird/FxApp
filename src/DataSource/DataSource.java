package DataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {

    private String url = "jdbc:mysql://sql2.freemysqlhosting.net:3306/sql2107839";
    private String login = "sql2107839";
    private String password = "rP1*eZ5!";
    private Connection connection;
    private static DataSource instance;

    private DataSource() {
        try {
            //properties = new Properties();
            //properties.load(new FileInputStream(new File("configuration.properties")));
            //url = properties.getProperty("url");
            //login = properties.getProperty("login");
            //password = properties.getProperty("password");
            connection = DriverManager.getConnection(url, login, password);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static DataSource getInstance() {
        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }

}
