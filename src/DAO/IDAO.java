/**
 * *********************************************************************
 * Module: Idao.java Author: Belguith Purpose: Defines the Interface Idao
 * *********************************************************************
 */
package DAO;

import javafx.collections.ObservableList;

public interface IDAO<T> {

    T findById(int id);

    ObservableList<T> findAll();

    T findOneBy(String criteria, int value);

    T findOneBy(String criteria, String value);

    ObservableList<T> findAllOrderBy(String orderColumn);

    ObservableList<T> findBy(String criteria, String value);

    ObservableList<T> findBy(String criteria, int value);

    boolean delete(int val);

    boolean update(T t);

    boolean insert(T t);

    boolean clean();

}
