/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataSource.DataSource;
import Entity.Comment;
import Entity.Request;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Belguith
 */
public class CommentDAO implements IDAO<Comment> {

    private final Connection db;
    private final Factory factory;

    public CommentDAO() {
        this.db = DataSource.getInstance().getConnection();
        this.factory = Factory.getInstance();
    }

    public ObservableList<Comment> findByInstructorCourse(int id) {
        String req = "SELECT comment.* FROM `comment`,fos_user,course where comment.fullname=fos_user.lastname and comment.course=course.id and course.instructor=fos_user.id and comment.modified>comment.created and fos_user.id=" + id;

        ObservableList<Comment> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Comment c = new Comment();
            while (result.next()) {
                c = new Comment(result.getString("id"), result.getDate("created"), result.getString("content"), result.getString("fullname"));
                c.setPicture(result.getString("picture"));
                c.setModified(result.getDate("modified"));
                if (result.getString("parent") != null) {
                    c.getParent().setId(result.getString("parent"));
                }
                c.getCourse().setId(result.getInt("course"));
                c.setHasupvote(result.getBoolean("hasupvote"));
                c.setUpvote(result.getInt("upvote"));
                list.add(c);

            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Comment findById(int id) {
        String req = "select * from comment where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Comment c = new Comment();

            if (result.next()) {
                c = new Comment(result.getString("id"), result.getDate("created"), result.getString("content"), result.getString("fullname"));
                c.setPicture(result.getString("picture"));
                c.setModified(result.getDate("modified"));
                if (result.getString("parent") != null) {
                    c.getParent().setId(result.getString("parent"));
                }
                c.getCourse().setId(result.getInt("course"));
                c.setHasupvote(result.getBoolean("hasupvote"));
                c.setUpvote(result.getInt("upvote"));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Comment> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment findOneBy(String criteria, int value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment findOneBy(String criteria, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Comment> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Comment> findBy(String criteria, String value) {
        ObservableList<Comment> list = FXCollections.observableArrayList();
        String req = "select * from comment where " + criteria + " ='" + value + "' ";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Comment c = new Comment();
            while (result.next()) {
                c = new Comment(result.getString("id"), result.getDate("created"), result.getString("content"), result.getString("fullname"));
                c.setPicture(result.getString("picture"));
                c.setModified(result.getDate("modified"));
                if (result.getString("parent") != null) {
                    c.getParent().setId(result.getString("parent"));
                }
                c.getCourse().setId(result.getInt("course"));
                c.setHasupvote(result.getBoolean("hasupvote"));
                c.setUpvote(result.getInt("upvote"));
                list.add(c);

            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Comment> findBy(String criteria, int value) {

        String req = "select * from comment where " + criteria + "=" + value;
        ObservableList<Comment> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Comment c = new Comment();
            while (result.next()) {
                c = new Comment(result.getString("id"), result.getDate("created"), result.getString("content"), result.getString("fullname"));
                c.setPicture(result.getString("picture"));
                c.setModified(result.getDate("modified"));
                if (result.getString("parent") != null) {
                    c.getParent().setId(result.getString("parent"));
                }
                c.getCourse().setId(result.getInt("course"));
                c.setHasupvote(result.getBoolean("hasupvote"));
                c.setUpvote(result.getInt("upvote"));
                list.add(c);

            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from comment where id=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Comment t) {
        String req = "update comment set parent='" + t.getParent() + "' hasupvote='" + t.isHasupvote() + "' modified='" + t.getModified() + "'  upvote='" + t.getUpvote() + "',  content='" + t.getContent() + "' "
                + "where id= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Comment t) {
        String req = "insert into comment (created,content,fullname,picture,course,parent) values (" + t.getCreated() + "," + t.getContent() + "," + t.getFullname() + "," + t.getPicture() + "," + t.getCourse().getId() + "," + t.getParent() + ")";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
