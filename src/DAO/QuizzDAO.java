/**
 * *********************************************************************
 * Module: QuizzDAO.java Author: Hiba Purpose: Defines the Class QuizzDAO
 * *********************************************************************
 */
package DAO;

import Entity.Quizz;

import DataSource.DataSource;
import Entity.Chapter;
import Entity.Skill;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class QuizzDAO implements IDAO<Quizz> {

    private final Connection db;

    public QuizzDAO() {
        db = DataSource.getInstance().getConnection();
        App.factory = Factory.getInstance();
    }
    @Override
    public Quizz findById(int id) {
        try {
            String req = "select * from quizz where id=" + id;
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q = new Quizz();

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findOneBy("ID", result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findOneBy("id", result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));
                q.setChapter(c);
                q.setSkill(s);

            }
            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Quizz> findAll() {
        try {
            ObservableList<Quizz> listquizz = FXCollections.observableArrayList();
            String req = "select * from quizz";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q;

            while (result.next()) {
                Chapter c =App.factory.getChapiter().findById(result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findById(result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));

                listquizz.add(q);
            }
            return listquizz;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Quizz findOneBy(String criteria, int value) {
        try {
            String req = "select * from quizz where " + criteria + "=" + value;

            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q = new Quizz();

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findOneBy("id", result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findOneBy("id", result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));
                q.setChapter(c);
                q.setSkill(s);

            }
            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Quizz findOneBy(String criteria, String value) {
        try {
            String req = "select * from quizz where " + criteria + "='" + value + "'";

            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q = new Quizz();

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findOneBy("id", result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findOneBy("id", result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));
                q.setChapter(c);
                q.setSkill(s);

            }
            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Quizz> findAllOrderBy(String orderColumn) {
        try {
            ObservableList<Quizz> listquizz = FXCollections.observableArrayList();
            String req = "select * from quizz ORDER BY " + orderColumn + " DESC ";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q;

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findById(result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findById(result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));

                listquizz.add(q);
            }
            return listquizz;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Quizz> findBy(String criteria, String value) {
        try {
            ObservableList<Quizz> listquizz = FXCollections.observableArrayList();
            String req = "select * from quizz where " + criteria + "='" + value + "'";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q;

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findById(result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findById(result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));

                listquizz.add(q);
            }
            return listquizz;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Quizz> findBy(String criteria, int value) {
        try {
            ObservableList<Quizz> listquizz = FXCollections.observableArrayList();
            String req = "select * from quizz where " + criteria + "=" + value;
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Quizz q;

            while (result.next()) {
                Chapter c = App.factory.getChapiter().findById(result.getInt("CHAPTER"));
                Skill s = App.factory.getSkill().findById(result.getInt("SKILL"));

                q = new Quizz(result.getString("TYPE"), result.getInt("LEVEL"), result.getString("TITLE"), result.getInt("DURATION"));
                q.setId(result.getInt("id"));

                listquizz.add(q);
            }
            return listquizz;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from quizz where id =" + val;;

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean update(Quizz t) {

        String req = "update quizz set TITLE= '" + t.getTitle() + "',DURATION= '" + t.getDuration() + "',TYPE= '" + t.getType() + "', LEVEL= '" + t.getLevel() + "'";

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean insert(Quizz t) {
        String req = "insert into quizz(DURATION,TITLE,SKLL,CHAPTER,TYPE,LEVEL) values('" + t.getDuration() + "','" + t.getTitle() + "','" + t.getSkill() + "','" + t.getChapter() + "','" + t.getType() + "', '" + t.getLevel() + "')";

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuizzDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
