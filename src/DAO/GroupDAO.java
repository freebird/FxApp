/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataSource.DataSource;
import Entity.Group;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Skander Fekih
 */
class GroupDAO implements IDAO<Group> {

    private Connection db;
    private final Factory factory;

    public GroupDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();

    }

    @Override
    public Group findById(int id) {
        String req = "select * from fos_group where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Group g = new Group();

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
            }
            return g;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Group> findAll() {
        ObservableList<Group> list = FXCollections.observableArrayList();
        String req = "select * from fos_group";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                Group g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
                list.add(g);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Group findOneBy(String criteria, int value) {
        String req = "select * from group where " + criteria + "=" + value;
        Statement statement;
        Group g = new Group();
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));

            }
            return g;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Group findOneBy(String criteria, String value) {
        String req = "select * from group where " + criteria + "='" + value + "'";
        Statement statement;
        Group g = new Group();
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
            }
            return g;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Group> findAllOrderBy(String orderColumn) {
        ObservableList<Group> list = FXCollections.observableArrayList();
        String req = "select * from group ORDER BY " + orderColumn + " DESC ";
        Group g;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
                list.add(g);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Group> findBy(String criteria, String value) {
        String req = "select * from group where " + criteria + "='" + value + "'";
        Statement statement;
        ObservableList<Group> list = FXCollections.observableArrayList();
        Group g;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
                list.add(g);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Group> findBy(String criteria, int value) {
        String req = "select * from group where " + criteria + "=" + value;
        Statement statement;
        ObservableList<Group> list = FXCollections.observableArrayList();
        Group g;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                g = new Group(result.getInt("id"), result.getString("name"), result.getString("roles"));
                list.add(g);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(GroupDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int val) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Group t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(Group t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
