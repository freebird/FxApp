/**
 * *********************************************************************
 * Module: CourseDAO.java Author: Cyrine Purpose: Defines the Class CourseDAO
 * *********************************************************************
 */
package DAO;

import Entity.Course;
import DataSource.DataSource;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CourseDAO implements IDAO<Course> {

    private final Connection db;

    public CourseDAO() {
        db = DataSource.getInstance().getConnection();
        App.factory = Factory.getInstance();
    }

    @Override
    public Course findById(int id) {
        String req = "select * from course where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setRate(result.getDouble("RATE"));
                c.setInstructor(App.factory.getInsctructor().findById(result.getInt("INSTRUCTOR")));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Course> findAll() {

        String req = "select * from course ";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Course> list = FXCollections.observableArrayList();
            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setRate(result.getDouble("RATE"));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Course findOneBy(String criteria, int value) {
        String req = "select * from course where " + criteria + "=" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setRate(result.getDouble("RATE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public ObservableList<Course> findTopCourses() {
        String req = "SELECT COUNT( subscription.COURSE ) as NBRSUB ,course.* FROM subscription,course where subscription.COURSE=course.id GROUP BY COURSE ORDER BY NBRSUB DESC";
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setRate(result.getDouble("RATE"));
                c.setInstructor(App.factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Course findOneBy(String criteria, String value) {
        String req = "select * from course where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setRate(result.getDouble("RATE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Course> findAllOrderBy(String orderColumn
    ) {

        String req = "select * from course ORDER BY " + orderColumn + " DESC ";
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Course c = new Course();

            if (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setRate(result.getDouble("RATE"));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Course> findBy(String criteria, String value
    ) {
        String req = "select * from course where " + criteria + "='" + value + "'";
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setRate(result.getDouble("RATE"));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Course> findBy(String criteria, int value) {
        String req = "select * from course where " + criteria + " = " + value;
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {
                Course c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setRate(result.getDouble("RATE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {

        String req = "delete from course where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public boolean update(Course t) {
        String req = "update course set TITLE='" + t.getTitle() + "', LEVEL='" + t.getLevel() + "',where ID= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Course t) {
        String req;
        java.sql.Date d = new java.sql.Date(t.getDuration().getTime());
        java.sql.Date d1 = new java.sql.Date(t.getDuration().getTime());

        req = "insert into course (INSTRUCTOR,TITLE,LEVEL,DURATION,DESCRIPTION,video_id,icon_id,AVAILABLE,BOOK,PUBLISHED) values (" + t.getInstructor().getId() + " '" + t.getTitle() + "','" + t.getLevel() + "' ,'" + d + "','" + t.getDescription() + "','" + t.getVideo().getId() + "','" + t.getIcon().getId() + "','" + 0 + "','" + t.getBook() + "','" + d1 + "') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ObservableList<Course> findByBlida(int val) {
        String req = "select course.* from course , subscription WHERE course.id=subscription.COURSE AND subscription.STUDENT= " + val + " AND subscription.TYPE = 'TRAINING'";
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public ObservableList<Course> findMyTopCourses(int id) {
        String req = "SELECT * from course where INSTRUCTOR =" + id + " ORDER BY RATE DESC LIMIT 5";
        ObservableList<Course> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Course c = new Course();

            while (result.next()) {
                c = new Course(result.getInt("ID"), result.getString("TITLE"), result.getInt("LEVEL"), result.getDate("DURATION"), result.getString("DESCRIPTION"), result.getString("BOOK"));
                c.setAvailable(result.getInt("AVAILABLE"));
                c.setVideo(App.factory.getVideo().findOneBy("id", result.getInt("video_id")));
                c.setRate(result.getDouble("RATE"));
                c.setInstructor(App.factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                c.setSubscription(App.factory.getSubscription().findBy("COURSE", c.getId()));
                c.setIcon(App.factory.getMedia().findOneBy("id", result.getInt("icon_id")));
                list.add(c);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

}
