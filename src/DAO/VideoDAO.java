/**
 * *********************************************************************
 * Module: VideoDAO.java Author: Cyrine Purpose: Defines the Class VideoDAO
 * *********************************************************************
 */
package DAO;

import Entity.Video;
import DataSource.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class VideoDAO implements IDAO<Video> {

    private final Connection db;
    private final Factory factory;

    public VideoDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Video findById(int id) {
        String req = "select * from video where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Video v = new Video();

            while (result.next()) {

                v = new Video(result.getInt("ID"), result.getString("NAME"), result.getString("URL"));

            }
            return v;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Video> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Video findOneBy(String criteria, int value) {
        String req = "select * from video where " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Video v = null;
            while (result.next()) {

                v = new Video(result.getInt("ID"), result.getString("NAME"), result.getString("URL"));

            }
            return v;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Video findOneBy(String criteria, String value) {
        String req = "select * from video where " + criteria + "  ='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Video v = null;
            while (result.next()) {

                v = new Video(result.getInt("ID"), result.getString("NAME"), result.getString("URL"));

            }
            return v;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Video> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Video> findBy(String criteria, String value) {
        ObservableList<Video> list = FXCollections.observableArrayList();
        String req = "select * from video where " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Video v = null;
            while (result.next()) {

                v = new Video(result.getInt("ID"), result.getString("NAME"), result.getString("URL"));

                list.add(v);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Video> findBy(String criteria, int value) {
        ObservableList<Video> list = FXCollections.observableArrayList();
        String req = "select * from video where " + criteria + "  =" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Video v = null;
            while (result.next()) {

                v = new Video(result.getInt("ID"), result.getString("NAME"), result.getString("URL"));

                list.add(v);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from video where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Video v) {
        String req = "update video set NAME='" + v.getName() + "', URL='" + v.getUrl() + "', RATE='" + v.getRate() + "',TAG1='" + v.getTag1() + "', TAG2='" + v.getTag2() + "', TAG3='" + v.getTag3() + "' "
                + "where ID= " + v.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Video v) {
        String req;
        req = "insert into video (NAME,URL) values ('" + v.getName() + "','" + v.getUrl() +"') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            System.out.println(result);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
