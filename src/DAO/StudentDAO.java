/**
 * *********************************************************************
 * Module: StudentDAO.java Author: Belguith Purpose: Defines the Class
 * StudentDAO
 * *********************************************************************
 */
package DAO;

import Entity.Student;
import DataSource.DataSource;
import Entity.Group;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class StudentDAO implements IDAO<Student> {

    private Connection db;
    private final Factory factory;

    public StudentDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Student findById(int id) {
        String req = "select * from fos_user where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Student u = new Student();

            while (result.next()) {
                u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;  //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Student> findAll() {
        ObservableList<Student> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%STUDENT%'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Student u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Student findOneBy(String criteria, int value) {
        ObservableList<Student> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where  roles LIKE '%STUDENT%' AND " + criteria + "=" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Student u = null;
            while (result.next()) {

                u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));

            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Student findOneBy(String criteria, String value) {
        ObservableList<Student> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%STUDENT%' AND " + criteria + "='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Student u = null;
            while (result.next()) {

                u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));

            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Student> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Student> findBy(String criteria, String value) {
        String req = "select * from fos_user where roles LIKE '%STUDENT%' AND " + criteria + "='" + value + "'";
        ObservableList<Student> list = FXCollections.observableArrayList();

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Student u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Student> findBy(String criteria, int value) {
        String req = "select * from fos_user where roles LIKE '%STUDENT%' AND " + criteria + "=" + value;
        ObservableList<Student> list = FXCollections.observableArrayList();

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Student u = new Student(result.getInt("id"), result.getString("firstname"), result.getString("lastname"), result.getString("email"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from fos_user where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Student t) {
        String req = "update fos_user set description='" + t.getDescription() + "roles='" + t.getRoles() + "enable='" + t.isEnabled() + "BIRTDAY='" + t.getDateOfBirth() + "',AVATAR='" + t.getAvatar() + "', EMAIL='" + t.getEmail() + ", "
                + "FIRSTNAME='" + t.getFirstName() + "', LASTNAME='" + t.getLastName() + "' where ID= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Student t) {
        Group g = factory.getGourp().findOneBy("name", "ORGANISM");
        String req1 = "INSERT INTO `users_groups`(`user_id`, `group_id`) VALUES (" + t.getId() + ", " + g.getId() + ") ";

        String req = "insert into fos_user (EMAIL,PASSWORD,username,LASTNAME,FIRSTNAME ,AVATAR,date_of_birth,enable,roles,DESCRIPTION) "
                + "values ('"
                + t.getEmail() + "',"
                + "'" + t.getPassword() + "' ,"
                + "'" + t.getUsername() + "',"
                + "'" + t.getLastName() + "',"
                + "'" + t.getFirstName() + "' ,"
                + "'" + t.getAvatar() + "',"
                + " '" + t.getDateOfBirth() + "',"
                + " '" + t.isEnabled() + "',"
                + " '" + g.getRoles() + "',"
                + "'" + t.getDescription() + " ) ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            int result1 = statement.executeUpdate(req1);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
