/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataSource.DataSource;
import Entity.Media;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Skander Fekih
 */
public class MediaDAO implements IDAO<Media> {

    private final Connection db;
    private final Factory factory;

    public MediaDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Media findById(int id) {

        String req = "select * from media where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
            }

            return m;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Media> findAll() {
        String req = "select * from media ";
        Statement statement;
        ObservableList<Media> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
                list.add(m);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Media findOneBy(String criteria, int value) {
        String req = "select * from media where " + criteria + "=" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
            }

            return m;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Media findOneBy(String criteria, String value) {
        String req = "select * from media where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
            }

            return m;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Media> findAllOrderBy(String orderColumn) {
        String req = "select * from media ORDER BY " + orderColumn + " DESC ";
        Statement statement;
        ObservableList<Media> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
                list.add(m);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Media> findBy(String criteria, String value) {
        String req = "select * from media where " + criteria + "='" + value + "'";
        Statement statement;
        ObservableList<Media> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
                list.add(m);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Media> findBy(String criteria, int value) {
        String req = "select * from media where " + criteria + "='" + value;
        Statement statement;
        ObservableList<Media> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Media m = new Media();

            if (result.next()) {
                m = new Media(result.getInt("id"), result.getString("name"), result.getString("path"));
                list.add(m);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from media where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Media t) {
        String req = "update media set name='" + t.getName() + "', path='" + t.getPath() + "',where id= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Media t) {
        String req = "insert into media (name,path) values ('" + t.getName() + "','" + t.getPath() + "') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MediaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
