/**
 * *********************************************************************
 * Module: OrganismDAO.java Author: Hiba Purpose: Defines the Class OrganismDAO
 * *********************************************************************
 */
package DAO;

import Entity.Organism;
import DataSource.DataSource;
import Entity.Group;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OrganismDAO implements IDAO<Organism> {

    private final Connection db;
    private final Factory factory;

    public OrganismDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Organism findById(int id) {
        String req = "select * from fos_user where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Organism u = new Organism();

            while (result.next()) {
                u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                //u.setRequests(factory.getRequest().findBy("ORGANISM", result.getInt("id")));
                u.setAvatar(App.factory.getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Organism> findAll() {
        ObservableList<Organism> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE %ORGANISM% ";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Organism u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Organism findOneBy(String criteria, int value) {
        String req = "select * from fos_user where roles LIKE '%ORAGNISM%' AND " + criteria + "  ='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Organism u = new Organism();

            while (result.next()) {
                u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(factory.getRequest().findBy("ORGANISM", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Organism findOneBy(String criteria, String value) {
        String req = "select * from fos_user where roles LIKE '%ORAGNISM%' AND " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Organism u = new Organism();

            while (result.next()) {
                u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(factory.getRequest().findBy("ORGANISM", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Organism> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Organism> findBy(String criteria, String value) {
        ObservableList<Organism> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%ORAGNISM%' AND " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Organism u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Organism> findBy(String criteria, int value) {
        ObservableList<Organism> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%ORAGNISM%' AND " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Organism u = new Organism(result.getInt("id"), result.getString("name"), result.getString("activity"), result.getString("phone"), result.getString("fax"), result.getString("address"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                //u.setSubcriptions(Factory.getInstance().getSubscriptionDAO().findBy("student", result.getInt("id")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from fos_user where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Organism t) {
        String req = "update fos_user set NAME='" + t.getName() + "', ACTIVITY=" + t.getActivity() + ", PHONE='" + t.getPhone() + "', FAX='" + t.getFax() + "',"
                + "ADDRESS='" + t.getAddress() + "' AVATAR='" + t.getAvatar() + "  where ID= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Organism t) {

        Group g = factory.getGourp().findOneBy("name", "ORGANISM");
        String req1 = "INSERT INTO `users_groups`(`user_id`, `group_id`) VALUES (" + t.getId() + ", " + g.getId() + ") ";

        String req = "insert into fos_user (EMAIL,PASSWORD,username,roles,NAME,ACTIVITY,AVATAR ,PHONE,FAX,ADDRESS) "
                + "values ('"
                + t.getEmail() + "',"
                + " '" + t.getPassword() + "' ,"
                + " '" + t.getUsername() + "' ,"
                + " '" + g.getRoles() + "' ,"
                + " '" + t.getName() + "',"
                + " '" + t.getActivity() + "' ,"
                + " '" + t.getAvatar() + "', "
                + "  " + t.getPhone() + ", "
                + " '" + t.getFax() + "', "
                + " '" + t.getAddress() + "' ) ";

        Statement statement;
        try {

            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            int result1 = statement.executeUpdate(req1);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
