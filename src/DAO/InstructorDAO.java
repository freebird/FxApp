package DAO;

import DataSource.DataSource;
import Entity.Group;
import Entity.Instructor;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InstructorDAO implements IDAO<Instructor> {

    private Connection db;

    public InstructorDAO() {
        db = DataSource.getInstance().getConnection();

    }

    public ObservableList<Instructor> findTopInstructor(int nbr) {
        String req = "SELECT * FROM `fos_user` where roles like '%INSTRUCTOR%' ORDER BY RATE DESC LIMIT " + nbr;
        ObservableList<Instructor> list = FXCollections.observableArrayList();
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Instructor i = new Instructor();

            while (result.next()) {
                i = new Instructor();
                i.setId(result.getInt("id"));
                i.setUsername(result.getString("username"));
                i.setAvatar(App.factory.getMedia().findOneBy("name", result.getString("username")));
                i.setCourses(App.factory.getCourse().findBy("INSTRUCTOR", result.getInt("id")));
                i.setDescription(result.getString("description"));
                list.add(i);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Instructor findById(int id) {
        String req = "select * from fos_user where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Instructor u = new Instructor();

            while (result.next()) {
                u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                u.setCourses(App.factory.getCourse().findBy("INSTRUCTOR", result.getInt("id")));
                u.setRate(result.getFloat("RATE"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Instructor> findAll() {
        ObservableList<Instructor> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%INSTRUCTOR%' ";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Instructor u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Instructor findOneBy(String criteria, int value) {
        String req = "select * from fos_user where roles LIKE '%INSTRUCTOR%' AND " + criteria + " =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Instructor u = null;

            while (result.next()) {
                u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Instructor findOneBy(String criteria, String value) {
        String req = "select * from fos_user where roles LIKE '%INSTRUCTOR%' AND " + criteria + "  = '" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Instructor u = null;

            while (result.next()) {
                u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Instructor> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Instructor> findBy(String criteria, String value) {
        ObservableList<Instructor> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%INSTRUCTOR%' AND " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Instructor u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Instructor> findBy(String criteria, int value) {
        ObservableList<Instructor> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where roles LIKE '%INSTRUCTOR%' AND " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Instructor u = new Instructor(result.getInt("id"), result.getString("phone"), result.getString("firstname"), result.getString("lastname"), result.getString("email"), result.getString("password"), result.getInt("experience"), result.getString("specialty"));
                u.setUsername(result.getString("username"));
                u.setPassword(result.getString("password"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setDescription(result.getString("description"));
                u.setRequests(App.factory.getRequest().findBy("INSTRUCTOR", result.getInt("id")));
                u.setCourses(App.factory.getCourse().findBy("INSTRUCTOR", result.getInt("id")));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from fos_user where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Instructor t) {
        String req = "update fos_user set firstname='" + t.getFirstName() + "', lastname=" + t.getLastName() + ", PHONE='" + t.getPhone() + "', SPECIALTY='" + t.getSpeciality() + "',"
                + "AVATAR='" + t.getAvatar() + "' where ID= " + t.getId();

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Instructor t) {

        Group g = App.factory.getGourp().findOneBy("name", "INSTRUCTOR");
        String req1 = "INSERT INTO `users_groups`(`user_id`, `group_id`) VALUES (" + t.getId() + ", " + g.getId() + ") ";

        String req = "insert into fos_user (EMAIL,PASSWORD,username,roles,LASTNAME,FIRSTNAME ,AVATAR,  date_of_birth ,DESCRIPTION,EXPERIENCE,SPECIALTY) "
                + "values ('"
                + t.getEmail() + "',"
                + " '" + t.getPassword() + "' ,"
                + " '" + t.getUsername() + "' ,"
                + " '" + g.getRoles() + "' ,"
                + " '" + t.getLastName() + "',"
                + " '" + t.getFirstName() + "' ,"
                + " '" + t.getAvatar() + "', "
                + "  " + t.getDateOfBirth() + ", "
                + " '" + t.getDescription() + "', "
                + "  " + t.getExperience() + ", "
                + " '" + t.getSpeciality() + "' ) ";

        Statement statement;
        try {

            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            int result2 = statement.executeUpdate(req1);

            System.out.println(result);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(InstructorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
