package DAO;

import DataSource.DataSource;

import Entity.Question;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class QuestionDAO implements IDAO<Question> {

     private final Connection db;

    public QuestionDAO() {
        db = DataSource.getInstance().getConnection();
        App.factory = Factory.getInstance();
    }

    @Override
    public Question findById(int id) {
        String req = "select * from question where id =" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            if (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
                
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", id));
                
            }

            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Question> findAll() {
        String req = "select * from question ";
        Statement statement;
        ObservableList<Question> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            if (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
                
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId() ));
                list.add(q);

            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Question findOneBy(String criteria, int value) {

        String req = "select * from question where " + criteria + "='" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            while (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
           
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId() ));

            }

            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Question findOneBy(String criteria, String value) {

        String req = "select * from question where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            if (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
               
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId()));

            }

            return q;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Question> findAllOrderBy(String orderColumn) {
        String req = "select * from question ORDER BY " + orderColumn + " DESC ";
        Statement statement;
        ObservableList<Question> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            if (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
                
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId()));
                list.add(q);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Question> findBy(String criteria, String value) {

        String req = "select * from question where " + criteria + "='" + value + "'";
        Statement statement;
        ObservableList<Question> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            while (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
                
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId()));
               
                list.add(q);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Question> findBy(String criteria, int value) {
        String req = "select * from question where " + criteria + "=" + value;
        Statement statement;
        ObservableList<Question> list = FXCollections.observableArrayList();

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Question q = new Question();

            while (result.next()) {
                q = new Question(result.getInt("id"),result.getInt("LEVEL"), result.getString("STATEMENT"));
               
                q.setQuizz(App.factory.getQuizz().findOneBy("id", result.getInt("quizz_id")));
                q.setResponse(App.factory.getResponse().findBy("question_id", q.getId()));
                 System.out.println(result.getInt("id"));
                list.add(q);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from question where id=" + val;;

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            System.out.println(result);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    @Override
    public boolean update(Question t) {
        String req = "update question set quizz_id= '" + t.getQuizz().getId() + "',LEVEL= '" + t.getLevel() + "',STATEMENT= '" + t.getStatement() + "'";

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            System.out.println(result);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean insert(Question t) {
        String req = "insert into question(quizz_id,STATEMENT,LEVEL) values('" + t.getQuizz().getId() + "','" + t.getStatement() + "','" + t.getLevel() + "')";

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            System.out.println(result);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(QuestionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
