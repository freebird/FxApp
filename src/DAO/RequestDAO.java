package DAO;

import Entity.Request;
import DataSource.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Skander
 */
public class RequestDAO implements IDAO<Request> {

    private final Connection db;
    private final Factory factory;

    public RequestDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Request findById(int id) {
        try {
            String req = "select * from request where id=" + id;
            Statement statement;
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r = new Request();

            while (result.next()) {
                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));

            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Request> findAll() {
        try {
            ObservableList<Request> list = FXCollections.observableArrayList();
            String req = "select * from request ";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r;

            while (result.next()) {

                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Request findOneBy(String criteria, int value) {
        try {
            String req = "select * from request where " + criteria + "=" + value;
            Statement statement;
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r = new Request();

            while (result.next()) {
                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));
            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Request findOneBy(String criteria, String value) {
        try {
            String req = "select * from request where " + criteria + "='" + value + "'";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r = new Request();

            while (result.next()) {

                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));

            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Request> findAllOrderBy(String orderColumn) {
        try {
            ObservableList<Request> list = FXCollections.observableArrayList();
            String req = "select * from request ORDER BY " + orderColumn + " DESC ";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r;

            while (result.next()) {
                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Request> findBy(String criteria, String value) {
        try {
            ObservableList<Request> list = FXCollections.observableArrayList();
            String req = "select * from request where " + criteria + "='" + value + "'";
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r = new Request();

            while (result.next()) {
                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.setInstructor(factory.getInsctructor().findOneBy("id", result.getInt("INSTRUCTOR")));
                r.setOrganism(factory.getOrganism().findOneBy("id", result.getInt("ORGANISM")));
                r.setSeen(result.getInt("seen"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ObservableList<Request> findBy(String criteria, int value) {
        try {
            ObservableList<Request> list = FXCollections.observableArrayList();
            String req = "select * from request where " + criteria + "=" + value;
            Statement statement;

            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Request r;

            while (result.next()) {
                r = new Request(result.getString("CONTENT"), result.getInt("TYPE"), result.getInt("STATUS"), result.getDate("DATE"));
                r.setId(result.getInt("id"));
                r.getInstructor().setId(result.getInt("INSTRUCTOR"));
                r.getOrganism().setId(result.getInt("ORGANISM"));
                r.setSeen(result.getInt("seen"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from request where ID=" + val;;

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    @Override
    public boolean update(Request t) {
        String req = "update request set LEVEL= '" + t.getStatus();

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean insert(Request t) {
        String req = "insert into request(INSTRUCTOR,ORGANISM,STATUS,DATE,CONTENT,TYPE) values('" + t.getInstructor().getId() + "','" + t.getOrganism().getId() + "','" + t.getStatus() + "',,'" + t.getDate() + "','" + t.getContent() + "',,'" + t.getType() + "')";

        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
