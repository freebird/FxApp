package DAO;

import DataSource.DataSource;
import Entity.Subscription;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Belguith
 */
public class SubscriptionDAO implements IDAO<Subscription> {

    private Connection db;
    private final Factory factory;

    public SubscriptionDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Subscription findById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Subscription> findAll() {
        ObservableList<Subscription> list = FXCollections.observableArrayList();
        String req = "select * from subscription";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Subscription u = new Subscription(result.getInt("id"), result.getDate("date"), result.getString("type"));
                u.getCourse().setId(result.getInt("COURSE"));
                u.getInstructor().setId(result.getInt("INSTRUCTOR"));
                u.getStudent().setId(result.getInt("STUDENT"));

                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SubscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Subscription findOneBy(String criteria, int value) {
        String req = "select * from subscription where " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Subscription u = null;
            while (result.next()) {

                u = new Subscription(result.getInt("id"), result.getDate("date"), result.getString("type"));
                u.getCourse().setId(result.getInt("COURSE"));
                u.getInstructor().setId(result.getInt("INSTRUCTOR"));
                u.getStudent().setId(result.getInt("STUDENT"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Subscription findOneBy(String criteria, String value) {
        String req = "select * from subscription where " + criteria + "  ='" + value + "' ";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Subscription u = null;
            while (result.next()) {

                u = new Subscription(result.getInt("id"), result.getDate("date"), result.getString("type"));
                u.getCourse().setId(result.getInt("COURSE"));
                u.getInstructor().setId(result.getInt("INSTRUCTOR"));
                u.getStudent().setId(result.getInt("STUDENT"));

            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(VideoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Subscription> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Subscription> findBy(String criteria, String value) {

        ObservableList<Subscription> list = FXCollections.observableArrayList();
        String req = "select * from subscription where " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Subscription u = new Subscription(result.getInt("id"), result.getDate("date"), result.getString("type"));
                u.getCourse().setId(result.getInt("COURSE"));
                u.getInstructor().setId(result.getInt("INSTRUCTOR"));
                u.getStudent().setId(result.getInt("STUDENT"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Subscription> findBy(String criteria, int value) {
        ObservableList<Subscription> list = FXCollections.observableArrayList();
        String req = "select * from subscription  where " + criteria + "  =" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Subscription u = new Subscription(result.getInt("id"), result.getDate("date"), result.getString("type"));
                u.getCourse().setId(result.getInt("COURSE"));
                u.getInstructor().setId(result.getInt("INSTRUCTOR"));
                u.getStudent().setId(result.getInt("STUDENT"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(OrganismDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from subscription where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SubscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Subscription t) {
        String req = "update subscription set STUDENT=" + t.getStudent().getId() + ", COURSE=" + t.getCourse().getId() + " , INSTRUCTOR=" + t.getInstructor().getId() + ", " + t.getSubDate() + "  where  ID=" + t.getId() + " ";
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SubscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Subscription t) {
        String req = "insert into subscription (COURSE,STUDENT,INSTRUCTOR,DATE,TYPE) values ( '" + t.getCourse().getId() + "', '" + t.getStudent().getId() + "', '" + t.getInstructor().getId() + "','" + t.getSubDate() + "' , '" + t.getType() + "' ) ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SubscriptionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
