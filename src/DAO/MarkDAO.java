/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DataSource.DataSource;
import Entity.Mark;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Belguith
 */
public class MarkDAO implements IDAO<Mark> {

    private Connection db;
    private final Factory factory;

    public MarkDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Mark findById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Mark> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Mark findOneBy(String criteria, int value) {
        String req = "select * from mark where " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Mark u = null;
            while (result.next()) {

                u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Mark findOneBy(String criteria, String value) {
        String req = "select * from mark where " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Mark u = null;
            while (result.next()) {

                u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Mark> findAllOrderBy(String orderColumn) {

        ObservableList<Mark> list = FXCollections.observableArrayList();
        String req = "select * from mark ORDER BY " + orderColumn + " DESC ";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Mark u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Mark> findBy(String criteria, String value) {

        ObservableList<Mark> list = FXCollections.observableArrayList();
        String req = "select * from mark  " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Mark u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Mark> findBy(String criteria, int value) {
        ObservableList<Mark> list = FXCollections.observableArrayList();
        String req = "select * from mark  " + criteria + "  =" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Mark u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from mark where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Mark t) {
        String req = "update mark set USER=" + t.getUser().getId() + ", QUIZZ=" + t.getQuizz().getId() + " , mark=" + t.getValue() + "where id=" + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Mark t) {
        String req = "insert into mark (USER,QUIZZ,mark) values ( '" + t.getUser().getId() + "', '" + t.getQuizz().getId() + "', '" + t.getValue() + "' ) ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ObservableList<Mark> findTopMarks(int id) {

        ObservableList<Mark> list = FXCollections.observableArrayList();
        String req = "select * from mark where USER="+id+" ORDER BY Mark DESC  LIMIT 5 ";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                Mark u = new Mark(result.getInt("id"), result.getDouble("mark"));
                u.setUser(factory.getUser().findById(result.getInt("user")));
                u.setQuizz(factory.getQuizz().findById(result.getInt("QUIZZ")));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(MarkDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

}
