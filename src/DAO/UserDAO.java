package DAO;

import Entity.User;
import DataSource.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class UserDAO implements IDAO<User> {

    private Connection db;
    private final Factory factory;

    public UserDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public User findById(int id) {
        String req = "select * from fos_user where id=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            User u = new User();

            while (result.next()) {
                u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<User> findAll() {
        ObservableList<User> list = FXCollections.observableArrayList();
        String req = "select * from fos_user";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            while (result.next()) {

                User u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User findOneBy(String criteria, int value) {
        ObservableList<User> list = FXCollections.observableArrayList();
        String req = "select * from fos_user where " + criteria + "=" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            User u = null;
            while (result.next()) {

                u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));

            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User findOneBy(String criteria, String value) {
        String req = "select * from fos_user where " + criteria + "='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            User u = null;
            while (result.next()) {

                u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));

            }
            return u;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<User> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<User> findBy(String criteria, String value) {
        ObservableList<User> list = FXCollections.observableArrayList();

        String req = "select * from fos_user where " + criteria + "='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            User u = null;
            while (result.next()) {

                u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);

            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<User> findBy(String criteria, int value) {
        ObservableList<User> list = FXCollections.observableArrayList();

        String req = "select * from fos_user where " + criteria + "=" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            User u = null;
            while (result.next()) {

                u = new User(result.getInt("id"), result.getString("username"), result.getString("email"), result.getString("password"), result.getString("description"));
                u.setEnabled(result.getBoolean("enabled"));
                u.setAvatar(Factory.getInstance().getMedia().findOneBy("name", result.getString("username")));
                u.setRoles(result.getString("roles"));
                list.add(u);

            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(int val) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean insert(User t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
