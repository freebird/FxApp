/**
 * *********************************************************************
 * Module: ChapiterDAO.java Author: Belguith Purpose: Defines the Class
 * ChapiterDAO
 * *********************************************************************
 */
package DAO;

import DataSource.DataSource;
import Entity.Chapter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ChapiterDAO implements IDAO<Chapter> {

    private final Connection db;
    private final Factory factory;

    public ChapiterDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Chapter findById(int id) {
        String req = "select * from chapter where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Chapter> findAll() {

        String req = "select * from chapter ";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Chapter> list = FXCollections.observableArrayList();
            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
                list.add(c);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public Chapter findOneBy(String criteria, int value) {
        String req = "select * from chapter where " + criteria + "=" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Chapter findOneBy(String criteria, String value) {
        String req = "select * from chapter where " + criteria + "='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
            }

            return c;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Chapter> findAllOrderBy(String orderColumn) {
        String req = "select * from chapter ORDER BY " + orderColumn + " DESC ";
        ObservableList<Chapter> list = FXCollections.observableArrayList();
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
                list.add(c);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Chapter> findBy(String criteria, String value) {
        String req = "select * from chapter where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Chapter> list = FXCollections.observableArrayList();
            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
                list.add(c);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    @Override
    public ObservableList<Chapter> findBy(String criteria, int value) {
        String req = "select * from chapter where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Chapter> list = FXCollections.observableArrayList();
            Chapter c = new Chapter();

            while (result.next()) {
                c = new Chapter(result.getString("TITLE"), result.getString("DESCRIPTION"), result.getString("PRESENTATION"), result.getString("VIDEO"), result.getString("CONTENT"), result.getInt("AVAILABLE"));
                c.setId(result.getInt("id"));
                c.setQuizz(factory.getQuizz().findBy("CHAPTER", c.getId()));
                list.add(c);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from chapter where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Chapter t) {
        String req = "update chapter set   AVAILABLE='" + t.getAvailable() + "',  CONTENT='" + t.getContent() + "' "
                + "where ID= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public boolean insert(Chapter t) {
        String req = "insert into chapter (TITLE,CONTENT,DESCRIPTION,VIDEO,AVAILABLE,PRESENTATION) values ('" + t.getTitle() + "','" + t.getContent() + "','" + t.getDescription() + "','" + t.getVideo() + "','" + t.getAvailable() + "','" + t.getPresentation() + "') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ChapiterDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
