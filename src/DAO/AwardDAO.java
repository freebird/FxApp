package DAO;

import DataSource.DataSource;
import Entity.Award;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AwardDAO implements IDAO<Award> {

    private final Connection db;
    ObservableList<Award> a;

    public AwardDAO() {
        db = DataSource.getInstance().getConnection();
        App.factory = Factory.getInstance();
    }

    @Override
    public Award findById(int id) {
        String req = "select * from award where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Award a = new Award();

            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));

            }

            return a;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Award> findAll() {

        String req = "select * from award ";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Award> list = FXCollections.observableArrayList();
            Award a = new Award();
            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));
                list.add(a);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Award findOneBy(String criteria, int value) {
        String req = "select * from award where " + criteria + "=" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Award a = new Award();

            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));

            }

            return a;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Award findOneBy(String criteria, String value) {
        String req = "select * from award where " + criteria + "='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Award a = new Award();

            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));
            }

            return a;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Award> findAllOrderBy(String orderColumn) {
        String req = "select * from award ORDER BY " + orderColumn + " DESC ";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Award> list = FXCollections.observableArrayList();
            Award a = new Award();
            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));
                list.add(a);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Award> findBy(String criteria, String value) {
        String req = "select * from award where " + criteria + "='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Award> list = FXCollections.observableArrayList();
            Award a = new Award();
            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));
                list.add(a);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Award> findBy(String criteria, int value) {
        String req = "select * from award where " + criteria + "=" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            ObservableList<Award> list = FXCollections.observableArrayList();
            Award a = new Award();
            while (result.next()) {
                a = new Award(result.getInt("ID"), result.getInt("LEVEL"));
                a.setStudent(App.factory.getStudent().findById(result.getInt("STUDENT")));
                a.setQuizz(App.factory.getQuizz().findById(result.getInt("QUIZZ")));
                a.setSkill(App.factory.getSkill().findById(result.getInt("SKILL")));
                list.add(a);
            }

            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {

        String req = "delete from award where ID=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    @Override
    public boolean update(Award t) {
        String req = "update award set ICON='" + t.getSkill().getIcon() + "', LEVEL='" + t.getLevel() + "', STUDENT='" + t.getStudent().getId() + "',QUIZZ='" + t.getQuizz().getId() + "',SKILL='" + t.getSkill().getId() + "',where ID= " + t.getId();
        Statement statement;

        try {
            statement = db.createStatement();
            statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Award t) {

        String req = "insert into award (ICON,LEVEL,STUDENT,QUIZZ,SKILL) values ('" + t.getSkill().getIcon() + "','" + t.getLevel() + "' ,'" + t.getStudent().getId() + "','" + t.getQuizz().getId() + "','" + t.getSkill().getId() + "') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AwardDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
