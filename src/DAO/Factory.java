/**
 * *********************************************************************
 * Module: DAOFactory.java Author: Belguith Purpose: Defines the Class
 * DAOFactory
 * *********************************************************************
 */
package DAO;

import DataSource.DataSource;

public class Factory {

    public static final DataSource connect = DataSource.getInstance();

    private RequestDAO requestDAO;
    private UserDAO userDAO;
    private StudentDAO studentDAO;
    private InstructorDAO instructorDAO;
    private OrganismDAO organismDAO;
    private AdministratorDAO administratorDAO;
    private CourseDAO courseDAO;
    private ChapiterDAO chapiterDAO;
    private AwardDAO awardDAO;
    private QuizzDAO quizzDAO;
    private SubscriptionDAO subscriptionDAO;
    private QuestionDAO questionDAO;
    private ResponseDAO responseDAO;
    private VideoDAO videoDAO;
    private SkillDAO skillDAO;
    private MediaDAO mediaDAO;
    private GroupDAO groupDAO;
    private MarkDAO markDAO;
    private CommentDAO commentDAO;
    private static Factory instance;

    public Factory() {

    }

    public static Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public StudentDAO getStudent() {
        if (studentDAO == null) {
            studentDAO = new StudentDAO();
        }
        return studentDAO;

    }

    public MarkDAO getMarkDAO() {
        if (markDAO == null) {
            markDAO = new MarkDAO();
        }
        return markDAO;
    }

    public GroupDAO getGourp() {
        if (groupDAO == null) {
            groupDAO = new GroupDAO();
        }
        return groupDAO;

    }

    public UserDAO getUser() {
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        return userDAO;

    }

    public RequestDAO getRequest() {
        if (requestDAO == null) {
            requestDAO = new RequestDAO();
        }
        return requestDAO;

    }

    public QuestionDAO getQuestion() {
        if (questionDAO == null) {
            questionDAO = new QuestionDAO();
        }
        return questionDAO;

    }

    public InstructorDAO getInsctructor() {
        if (instructorDAO == null) {
            instructorDAO = new InstructorDAO();
        }
        return new InstructorDAO();
    }

    public SubscriptionDAO getSubscription() {
        if (subscriptionDAO == null) {
            subscriptionDAO = new SubscriptionDAO();
        }
        return new SubscriptionDAO();
    }

    public OrganismDAO getOrganism() {
        if (organismDAO == null) {
            organismDAO = new OrganismDAO();
        }
        return organismDAO;
    }

    public AdministratorDAO getAdministrator() {
        if (administratorDAO == null) {
            administratorDAO = new AdministratorDAO();
        }
        return administratorDAO;
    }

    public CourseDAO getCourse() {
        if (courseDAO == null) {
            courseDAO = new CourseDAO();
        }
        return courseDAO;

    }

    public MediaDAO getMedia() {
        if (mediaDAO == null) {
            mediaDAO = new MediaDAO();
        }
        return mediaDAO;

    }

    public ChapiterDAO getChapiter() {
        if (chapiterDAO == null) {
            chapiterDAO = new ChapiterDAO();
        }
        return chapiterDAO;

    }

    public AwardDAO getAward() {
        if (awardDAO == null) {
            awardDAO = new AwardDAO();
        }
        return awardDAO;

    }

    public QuizzDAO getQuizz() {
        if (quizzDAO == null) {
            quizzDAO = new QuizzDAO();
        }
        return quizzDAO;

    }

    public ResponseDAO getResponse() {

        if (responseDAO == null) {
            responseDAO = new ResponseDAO();
        }
        return responseDAO;
    }

    public VideoDAO getVideo() {
        if (videoDAO == null) {
            videoDAO = new VideoDAO();
        }

        return videoDAO;
    }

    public SkillDAO getSkill() {
        if (skillDAO == null) {
            skillDAO = new SkillDAO();
        }
        return skillDAO;
    }

    public CommentDAO getComment() {
        if (commentDAO == null) {
            commentDAO = new CommentDAO();
        }
        return commentDAO;
    }

}
