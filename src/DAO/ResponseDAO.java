package DAO;

import Entity.Response;
import DataSource.DataSource;
import Entity.Question;
import fxapp.App;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ResponseDAO implements IDAO<Response> {
    
    private final Connection db;
    
    public ResponseDAO() {
        db = DataSource.getInstance().getConnection();
        App.factory = Factory.getInstance();
    }
    
    @Override
    public Response findById(int id) {
        try {
            String req = "select * from response where ID=" + id;
            Statement statement;
            
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                
            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
    @Override
    public ObservableList<Response> findAll() {
        try {
            String req = "select * from response";
            Statement statement;
            ObservableList<Response> list = FXCollections.observableArrayList();
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public Response findOneBy(String criteria, int value) {
        try {
            String req = "select * from response where " + criteria + "=" + value;
            
            Statement statement;
            
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                
            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public Response findOneBy(String criteria, String value) {
        try {
            String req = "select * from response where " + criteria + "='" + value + "'";
            
            Statement statement;
            
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                
            }
            return r;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public ObservableList<Response> findAllOrderBy(String orderColumn) {
        try {
            String req = "select * from response ORDER BY " + orderColumn + " DESC ";
            Statement statement;
            ObservableList<Response> list = FXCollections.observableArrayList();
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public ObservableList<Response> findBy(String criteria, String value) {
        try {
            String req = "select * from response where " + criteria + "='" + value + "'";
            Statement statement;
            ObservableList<Response> list = FXCollections.observableArrayList();
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                Question q = App.factory.getQuestion().findOneBy("ID", result.getInt("question_id"));
                
                r = new Response(result.getInt("ID"), result.getString("STATEMENT"), result.getString("TYPE"));
                r.setQuestion(q);
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public ObservableList<Response> findBy(String criteria, int value) {
        try {
            String req = "select * from response where " + criteria + "=" + value;
            Statement statement;
            ObservableList<Response> list = FXCollections.observableArrayList();
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                r = new Response(result.getInt("id"), result.getString("STATEMENT"), result.getString("TYPE"));
                System.out.println(" question_id==> " + result.getInt("question_id"));
                
                r.getQuestion().setId(result.getInt("question_id"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public ObservableList<String> findValidResponse(String criteria, int value) {
          ObservableList<String> list = FXCollections.observableArrayList();
     
    
        String req = "select * from response  where " + criteria + "=" + value + " and TYPE='CORRECT'"  ;
        Statement statement;
          try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Response r = new Response();
            
            while (result.next()) {
                r = new Response(result.getInt("id"), result.getString("STATEMENT"), result.getString("TYPE"));
                System.out.println(" question_id==> " + result.getInt("question_id"));
                
                r.getQuestion().setId(result.getInt("question_id"));
                list.add(r.getStatement());
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
    @Override
    public boolean delete(int val) {
        String req = "delete from response where ID=" + val;;
        
        Statement statement;
        
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean update(Response t) {
        String req = "update response set question_id= '" + t.getQuestion().getId() + "',TYPE= '" + t.getType() + "',STATEMENT= '" + t.getStatement() + "'";
        
        Statement statement;
        
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean insert(Response t) {
        String req = "insert into question(question_id,STATEMENT,TYPE) values('" + t.getQuestion().getId() + "','" + t.getStatement() + "','" + t.getType() + "')";
        
        Statement statement;
        
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ResponseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
