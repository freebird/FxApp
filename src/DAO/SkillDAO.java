package DAO;

import Entity.Skill;
import DataSource.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SkillDAO implements IDAO<Skill> {

    private Connection db;
    private final Factory factory;

    public SkillDAO() {
        db = DataSource.getInstance().getConnection();
        factory = Factory.getInstance();
    }

    @Override
    public Skill findById(int id) {
        String req = "select * from skill where ID=" + id;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Skill s = new Skill();

            while (result.next()) {

                s = new Skill(result.getInt("ID"), result.getString("NAME"), result.getString("icon"), result.getInt("value"));

            }
            return s;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Skill> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Skill findOneBy(String criteria, int value) {
        String req = "select * from skill where " + criteria + "  =" + value;
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Skill s = null;
            while (result.next()) {

                s = new Skill(result.getInt("ID"), result.getString("NAME"), result.getString("icon"), result.getInt("value"));

            }
            return s;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Skill findOneBy(String criteria, String value) {
        String req = "select * from skill where " + criteria + "  ='" + value + "'";
        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);

            Skill s = null;
            while (result.next()) {

                s = new Skill(result.getInt("ID"), result.getString("NAME"), result.getString("icon"), result.getInt("value"));

            }
            return s;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Skill> findAllOrderBy(String orderColumn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ObservableList<Skill> findBy(String criteria, String value) {
        ObservableList<Skill> list = FXCollections.observableArrayList();
        String req = "select * from skill where " + criteria + "  ='" + value + "'";

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Skill s = null;
            while (result.next()) {

                s = new Skill(result.getInt("ID"), result.getString("NAME"), result.getString("icon"), result.getInt("value"));

                list.add(s);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public ObservableList<Skill> findBy(String criteria, int value) {
        ObservableList<Skill> list = FXCollections.observableArrayList();
        String req = "select * from skill where " + criteria + "  =" + value;

        Statement statement;
        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            Skill s = null;
            while (result.next()) {

                s = new Skill(result.getInt("ID"), result.getString("NAME"), result.getString("icon"), result.getInt("value"));

                list.add(s);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public boolean delete(int val) {
        String req = "delete from Skill where id=" + val;
        Statement statement;

        try {
            statement = db.createStatement();
            ResultSet result = statement.executeQuery(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean update(Skill a) {
        String req = "update Skill set name= '" + a.getName() + "', icon= '" + a.getIcon() + "', value= '" + a.getValue();
        Statement statement;

        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean insert(Skill a) {
        String req = "insert into Skill (name,icon,value) values ('" + a.getName() + "','" + a.getIcon() + "','" + a.getValue() + "') ";
        Statement statement;
        try {
            statement = db.createStatement();
            int result = statement.executeUpdate(req);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean clean() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
