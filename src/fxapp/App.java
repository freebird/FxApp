/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fxapp;

import DAO.Factory;
import Entity.User;
import GUI.Controller.LoginInterfaceController;
import com.sun.javafx.application.LauncherImpl;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Belguith
 */
public class App extends Application {

    private static final double WIDTH = 1360;
    private static final double HEIGHT = 600;
    public static Factory factory = new Factory();
    private static final int COUNT_LIMIT = 6000;
    private static int stepCount = 1;

    public static Stage window = new Stage();
    public static User user = new User();

    // Used to demonstrate step couns.
    public static String STEP() {
        return stepCount++ + ". ";
    }

    public Parent loadLoginInterface() {
        URL location = LoginInterfaceController.class.getResource("/GUI/LoginInterface.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

        //fxmlLoader.setController(this);
        Parent root = null;
        try {
            System.out.println("loader ===> " + location.getPath());
            root = (Parent) fxmlLoader.load();

        } catch (IOException ex) {
            Logger.getLogger(LoginInterfaceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return root;
    }

    public App() {
        // Constructor is called after BEFORE_LOAD.
        System.out.println(App.STEP() + "MyApplication constructor called, thread: " + Thread.currentThread().getName());

    }

    @Override
    public void init() throws Exception {
        System.out.println(App.STEP() + "MyApplication#init (doing some heavy lifting), thread: " + Thread.currentThread().getName());
        System.out.println("'im loading My FXML files Here");

        System.out.println("login interface loaded");
        for (int i = 0; i < COUNT_LIMIT; i++) {
            double progress = (100 * i) / COUNT_LIMIT;
            LauncherImpl.notifyPreloader(this, new Preloader.ProgressNotification(progress));
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println(App.STEP() + "MyApplication#start (initialize and show primary application stage), thread: " + Thread.currentThread().getName());
        App.window.setScene(new Scene(loadLoginInterface()));
        //App.window.initStyle(StageStyle.UNDECORATED);
        App.window.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Main");
        LauncherImpl.launchApplication(App.class, AppPreloader.class, args);
    }

}
